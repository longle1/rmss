function tau = true_tdoas(fname)

% TRUE_TDOAS Retrieve the true TDOAs corresponding to a given set of mixing
% filters
%
% tau = true_tdoas(fname)
%
% Input
% fname: BSS Locate mixing filter filename
%
% Output
% tau: 1 x nsrc vector of true TDOAs in seconds
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2010-2011 Charles Blandin and Emmanuel Vincent
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt)
% If you find it useful, please cite the following reference:
% Charles Blandin, Emmanuel Vincent and Alexey Ozerov, "Multi-source TDOA
% estimation in reverberant audio using angular spectra and clustering",
% Signal Processing, to appear.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nsrc = str2double(fname(1));
load(fname, 'Srcpos', 'senspos1', 'senspos2');

tau = zeros(1,nsrc);
for j = 1:nsrc,
    tau(j) = (norm(Srcpos(:,j)-senspos2) - norm(Srcpos(:,j)-senspos1))/343;
end

return;