function [thresh,V,thresh2,V2,VE2,VI2] = optMultiSys(cascade,cascade2)
% [thresh,V,thresh2,V2] = optMultiSys(cascade1,cascade2)
% Find optimal thresholds/operating points for multiple applications
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

K = numel(cascade.D); % number of stages
M = size(cascade.b,2); % number of state discretization

% convert into robust observation densities
[cascade] = robustify(cascade);
piL = [cascade.initbL zeros(1,K-1)];
piU = [cascade.initbU zeros(1,K-1)];
for k = 1:K-1
    [piL(k+1),piU(k+1)] = robustRange(cascade,k,piL(k),piU(k));
end

[cascade2] = robustify(cascade2);
piL2 = [cascade2.initbL zeros(1,K-1)];
piU2 = [cascade2.initbU zeros(1,K-1)];
for k = 1:K-1
    [piLTmp,piUTmp] = robustRange(cascade,k,piL2(k),piU2(k));
    [piLTmp2,piUTmp2] = robustRange(cascade2,k,piL2(k),piU2(k));
    piL2(k+1) = min(piLTmp,piLTmp2);
    piU2(k+1) = max(piUTmp,piUTmp2);
end

V = zeros(M,K); % value functions
thresh = zeros(1,K);
V(:,K) = min(cascade.CM*cascade.b,cascade.CA*(1-cascade.b),'includenan');
thresh(K) = cascade.CA/(cascade.CA+cascade.CM);

[B2,B] = meshgrid(cascade.b, cascade2.b); % swap output order to avoid transpose
V2 = zeros(M,M,K);
VE2 = zeros(M,M,K);
VI2 = zeros(M,M,K);
thresh2 = zeros(M,M,K);
[minJ,minJIdx] = matEleMin(cascade2.CM*B2,cascade2.CA*(1-B2));
V2(:,:,K) = minJ;
VE2(:,:,K) = 0;
VI2(:,:,K) = minJ;
minJIdx(isnan(minJ)) = NaN;
thresh2(:,:,K) = minJIdx;
for k = K-1:-1:1
    % ========= app 1
    J = eval(cascade,k,V,piL,piU);
    
    % update V(k)
    V(:,k) = min(cascade.CM*cascade.b',cascade.D(k+1)+J,'includenan');
    V(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    
    % solve for thresh(k)
    fd = V(:,k)-cascade.CM*cascade.b'; % diff function
    idx = find(fd<0,1);
    thresh(k) = cascade.b(idx);
    
    % ========= app 2
    J_2 = eval2(cascade,k,V2,piL2,piU2,2); J_2(1:idx,:) = Inf;
    J22 = eval2(cascade2,k,V2,piL2,piU2,2);
    
    [minJ,minJIdx] = matEleMin(cascade2.CM*B2,cascade2.D(k+1)+J22,J_2);
    minJ(cascade.b<piL(k+1) | cascade.b>piU(k+1),:) = NaN;
    minJ(:,cascade2.b<piL2(k+1) | cascade2.b>piU2(k+1)) = NaN;
    V2(:,:,k) = minJ;
    
    % solve for thresh(k)
    minJIdx(isnan(minJ)) = NaN;
    minJIdx(cascade.b<piL(k+1) | cascade.b>piU(k+1),:) = NaN;
    minJIdx(:,cascade2.b<piL2(k+1) | cascade2.b>piU2(k+1)) = NaN;
    thresh2(:,:,k) = minJIdx;
    
    % energy & inference component
    JE_2 = eval2(cascade,k,VE2,piL2,piU2,2); JE_2(1:idx,:) = Inf;
    JE22 = eval2(cascade2,k,VE2,piL2,piU2,2);
    
    minJE = zeros(size(JE22));
    minJE(minJIdx == 2) = cascade2.D(k+1)+JE22(minJIdx == 2);
    minJE(minJIdx == 3) = JE_2(minJIdx == 3);
    minJE(cascade.b<piL(k+1) | cascade.b>piU(k+1),:) = NaN;
    minJE(:,cascade2.b<piL2(k+1) | cascade2.b>piU2(k+1)) = NaN;
    VE2(:,:,k) = minJE;
    
    JI_2 = eval2(cascade,k,VI2,piL2,piU2,2); JI_2(1:idx,:) = Inf;
    JI22 = eval2(cascade2,k,VI2,piL2,piU2,2);
    
    minJI = zeros(size(JI22));
    minJI(minJIdx == 1) = cascade2.CM*B2(minJIdx == 1);
    minJI(minJIdx == 2) = JI22(minJIdx == 2);
    minJI(minJIdx == 3) = JI_2(minJIdx == 3);
    minJI(cascade.b<piL(k+1) | cascade.b>piU(k+1),:) = NaN;
    minJI(:,cascade2.b<piL2(k+1) | cascade2.b>piU2(k+1)) = NaN;
    VI2(:,:,k) = minJI;
    
    % sharing feature test
    if sum(sum(J_2(idx+1:end,:) >= cascade2.D(k+1)+J22(idx+1:end,:))) == 0
        fprintf(1,'sharing feature in stage %d is optimal!\n',k);
    else
        warning('sharing feature in stage %d is NOT optimal!',k);
    end
end
% update V(0)
% ========= app 1
% energy cost of the first stage
J = eval(cascade,0,V,piL,piU);
    
V0 = cascade.D(1)+J;
V0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

V = [V0 V];

% ========= app 2
J_2 = eval2(cascade,0,V2,piL2,piU2,2);
J22 = eval2(cascade2,0,V2,piL2,piU2,2);

[minJ,minJIdx] = matEleMin(cascade2.D(1)+J22,J_2); minJIdx = 1+minJIdx;
minJ(cascade.b<piL(1) | cascade.b>piU(1),:) = NaN;
minJ(:,cascade2.b<piL2(1) | cascade2.b>piU2(1)) = NaN;
V20 = minJ;
V2 = cat(3,V20,V2);

minJIdx(isnan(minJ)) = NaN;
minJIdx(cascade.b<piL(1) | cascade.b>piU(1),:) = NaN;
minJIdx(:,cascade2.b<piL2(1) | cascade2.b>piU2(1)) = NaN;
thresh20 = minJIdx;
thresh2 = cat(3,thresh20,thresh2);

if sum(sum(J_2 >= cascade2.D(1)+J22)) == 0
    fprintf(1,'sharing feature in stage 0 is optimal!\n');
else
    warning('sharing feature in stage 0 is NOT optimal!');
end

% energy & inference component
JE_2 = eval2(cascade,0,VE2,piL2,piU2,2);
JE22 = eval2(cascade2,0,VE2,piL2,piU2,2);

minJE = zeros(size(JE22));
minJE(minJIdx==2) = cascade2.D(1)+JE22(minJIdx==2);
minJE(minJIdx==3) = JE_2(minJIdx==3);
minJE(cascade.b<piL(1) | cascade.b>piU(1),:) = NaN;
minJE(:,cascade2.b<piL2(1) | cascade2.b>piU2(1)) = NaN;
VE20 = minJE;
VE2 = cat(3,VE20,VE2);

JI_2 = eval2(cascade,0,VI2,piL2,piU2,2);
JI22 = eval2(cascade2,0,VI2,piL2,piU2,2);

minJI = zeros(size(JI22));
minJI(minJIdx==2) = JI22(minJIdx==2);
minJI(minJIdx==3) = JI_2(minJIdx==3);
minJI(cascade.b<piL(1) | cascade.b>piU(1),:) = NaN;
minJI(:,cascade2.b<piL2(1) | cascade2.b>piU2(1)) = NaN;
VI20 = minJI;
VI2 = cat(3,VI20,VI2);

end

function J = eval(cascade,k,V,piL,piU) 
    L = size(cascade.P0{k+1},2); % assume P0 and P1 has the same size 
    M = size(cascade.b,2);
    Q = zeros(M,L); % future reward
    parfor m = 1:M
        if cascade.b(m)>=piL(k+1) && cascade.b(m)<=piU(k+1)
            for l = 1:L
                [bb,pp] = stateObsProb(cascade,k+1,l,m);
                idx = nearestIdx(cascade.b,bb);
                Q(m,l) = pp*V(idx,k+1);
            end
        end
    end
    J = sum(Q,2);
end

function J = eval2(cascade,k,V,piL,piU,mainDim) 
    if mainDim == 2
        V = permute(V,[2 1 3]);
    end
    L = size(cascade.P0{k+1},2);
    M = size(cascade.b,2);
    Q = zeros(M,M,L); % future reward
    parfor n = 1:M
        for m = 1:M
            if cascade.b(m)>=piL(k+1) && cascade.b(m)<=piU(k+1)
                for l = 1:L
                    [bb,pp] = stateObsProb(cascade,k+1,l,m);
                    idx = nearestIdx(cascade.b,bb);
                    Q(m,n,l) = pp*V(idx,n,k+1);
                end
            end
        end
    end
    
    if mainDim == 2
        Q = permute(Q,[2 1 3]);
    end
    J = sum(Q,3);
end
