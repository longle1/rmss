function sysPlotV(cascade,V)
% plot system performance
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

fig = figure;
[R,t] = AxelRot(-atan(sqrt(2))/pi*180,[-1 1 0],[1 0 0]);
bb = R*cascade.b+repmat(t,1,size(cascade.b,2));
tri = delaunay(bb(1,:),bb(2,:));
h = trisurf(tri,bb(1,:),bb(2,:),V(:,1));

eIdx1 = find(cascade.b(1,:)==1);
text(bb(1,eIdx1),bb(2,eIdx1),'X=1','fontsize',15)
eIdx2 = find(cascade.b(2,:)==1);
text(bb(1,eIdx2),bb(2,eIdx2),'X=2','fontsize',15)
eIdx3 = find(cascade.b(3,:)==1);
text(bb(1,eIdx3)-0.2,bb(2,eIdx3)-0.2,'X=3','fontsize',15)

grid on
set(gca,'xcolor',get(fig,'color'),'xtick',[],'ytick',[]);
l = light('Position',[-50 -15 29]);
set(gca,'CameraPosition',[208 -50 7687])
lighting phong
shading interp
colorbar EastOutside
%view(-5,67)
view(115,65)
title([sprintf('||R||_1 = %.3f',norm(V(:,1),1)) ', \lambda = 0.005'])
zlabel('System Risk')
set(gca,'fontsize',15);