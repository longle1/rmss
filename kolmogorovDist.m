function dist = kolmogorovDist(p1,p2)
% Compute Kolmogorov distance of the two probability mass function
%
% Long Le
% University of Illinois
%

P1 = cumsum(p1);
P2 = cumsum(p2);
dist = max(abs(P1 - P2));