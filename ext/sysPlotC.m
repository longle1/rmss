function sysPlotC(cascade,C)
% plot system performance
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

idx{1} = find(C(:,1) == 1); idxN{1} = find(C(:,1) == 0);
idx{2} = find(C(:,2) == 1); idxN{2} = find(C(:,2) == 0);
%{
figure; hold on;
plot3(cascade.b(1,:),cascade.b(2,:),cascade.b(3,:),'rx')
plot3(cascade.b(1,idx{1}),cascade.b(2,idx{1}),cascade.b(3,idx{1}),'g^')
plot3(cascade.b(1,idx{2}),cascade.b(2,idx{2}),cascade.b(3,idx{2}),'bo')
legend('C_1 = 0, C_2 = 0','C_1 = 1, C_2 = 0','C_1 = 1, C_2 = 1')
axis vis3d
zoom(0.8);
grid on;
%}
% plot censoring region
figure
subplot(121);hold on;
plot3(cascade.b(1,idxN{1}),cascade.b(2,idxN{1}),cascade.b(3,idxN{1}),'rx')
xlabel('\pi(1)');ylabel('\pi(2)');zlabel('\pi(3)')
plot3(cascade.b(1,idx{1}),cascade.b(2,idx{1}),cascade.b(3,idx{1}),'bo')
xlabel('\pi(1)');ylabel('\pi(2)');zlabel('\pi(3)')
legend('C_1 = 0','C_1 = 1')
title('censoring region 1')
view(105,30);
grid on;
subplot(122);hold on;
plot3(cascade.b(1,idxN{2}),cascade.b(2,idxN{2}),cascade.b(3,idxN{2}),'rx')
xlabel('\pi(1)');ylabel('\pi(2)');zlabel('\pi(3)')
plot3(cascade.b(1,idx{2}),cascade.b(2,idx{2}),cascade.b(3,idx{2}),'bo')
xlabel('\pi(1)');ylabel('\pi(2)');zlabel('\pi(3)')
legend('C_2 = 0','C_2 = 1')
title('censoring region 2')
view(105,30);
grid on;