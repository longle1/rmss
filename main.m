% Cascade on synthetic data
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

%clear all; close all;
clear all;

%% find optimal thresholds
% Generate feature models
lambda = 0.0040;
cascade.D = [84.36*0.016 1097*0.011+15131*0.34e-6 15131*0.014]*lambda/3.6;
K = numel(cascade.D);
cascade.accDO = cumsum([nan 264*0.34e-6 264*0.014]*lambda/3.6, 2, 'reverse');
cascade.CM = 3;
cascade.CA = 1;
[cascade.P0,cascade.P1] = getPMFs();
cascade.b = (0:1/(2e2-1):1); % dummy probability
cascade.eps0 = [0.1 0.1];
cascade.eps1 = [0.1 0.1];
%cascade.nu0 = kolmogorovDist(cascade.P0{k},cascade.P0{K});
cascade.nu0 = [0.1 0.1];
%cascade.nu1 = kolmogorovDist(cascade.P1{k},cascade.P1{K})/5;
cascade.nu1 = [0.1 0.1];
cascade.initbL = 0.05;
cascade.initbU = 0.15;

[thresh,V] = optSys(cascade);
q = cascade.b;
isMulti = false;
figure; h = suptitle(['\lambda = ' num2str(lambda)]); set(h,'FontSize',18)
for k = 1:K+1
    if k==K+1
        subplot(2,2,k); hold on; plot(q,cascade.CM*q,'r'); plot(q,cascade.CA*(1-q),'g--'); plot(q,V(:,k),'b','linewidth',5);
        if isMulti
            legend(sprintf('C_M^1\\pi_%d^1',K),sprintf('C_A^1(1-\\pi_%d^1)',K),sprintf('V_%d^1',k-1),'location','northwest')
        else
            legend(sprintf('C_M\\pi_%d',K),sprintf('C_A(1-\\pi_%d)',K),sprintf('V_%d',k-1),'location','northwest')
        end
    elseif k == 1
        subplot(2,2,k); hold on; plot(q,V(:,k),'b','linewidth',5);
        if isMulti
            legend(sprintf('V_%d^1',k-1),'location','northwest')
        else
            legend(sprintf('V_%d',k-1),'location','northwest')
        end
    elseif isfield(cascade,'accDO') && cascade.accDO(k) ~= 0
        subplot(2,2,k); hold on; plot(q,cascade.CM*q+cascade.accDO(k),'r'); plot(q,cascade.CA*(1-q)+cascade.accDO(k),'g--'); plot(q,V(:,k),'b','linewidth',5);
        if isMulti
            legend(sprintf('C_M^1\\pi_%d^1+\\lambda\\Sigma_{j=%d}^3 D_j^{1,off}',k-1,k),sprintf('C_A^1(1-\\pi_%d^1)+\\lambda\\Sigma_{j=%d}^3 D_j^{1,off}',k-1,k),sprintf('V_%d^1',k-1),'location','northwest')
        else
            legend(sprintf('C_M\\pi_%d+\\lambda\\Sigma_{j=%d}^3 D_j^{off}',k-1,k),sprintf('C_A(1-\\pi_%d)+\\lambda\\Sigma_{j=%d}^3 D_j^{off}',k-1,k),sprintf('V_%d',k-1),'location','northwest')
        end
    else
        subplot(2,2,k); hold on; plot(q,cascade.CM*q,'r'); plot(q,cascade.CA*(1-q),'g--'); plot(q,V(:,k),'b','linewidth',5);
        if isMulti
            legend(sprintf('C_M^1\\pi_%d^1',k-1),sprintf('C_A^1(1-\\pi_%d^1)',k-1),sprintf('V_%d^1',k-1),'location','northwest')
        else
            legend(sprintf('C_M\\pi_%d',k-1),sprintf('C_A(1-\\pi_%d)',k-1),sprintf('V_%d',k-1),'location','northwest')
        end
    end
    if isMulti
        xlabel(sprintf('\\pi_%d^1',k-1)); ylabel('Risk')
    else
        xlabel(sprintf('\\pi_%d',k-1)); ylabel('Risk')
    end
    set(gca,'FontSize',15)
end

figure; h = suptitle(['\lambda = ' num2str(lambda)]); set(h,'FontSize',18)
act = zeros(size(V,1),K);
for k = 1:K
    subplot(3,1,k);
    % compute the decision/action function
    act(q >= thresh(k),k) = 1;
    act(isnan(V(:,k+1)),k) = nan;
    plot(q,act(:,k),'linewidth',5)
    if isMulti
        xlabel(sprintf('\\pi_%d^1',k)); ylabel(sprintf('\\delta_%d^{1\\ast}',k))
    else
        xlabel(sprintf('\\pi_%d',k)); ylabel(sprintf('\\delta_%d^{\\ast}',k))
    end
    
    qRange = q(~isnan(V(:,k+1)));
    if thresh(k) - min(qRange) < 0.05
        set(gca, 'XTick',[thresh(k),max(qRange)])
        set(gca, 'XTickLabel',{sprintf('%.2f',thresh(k)),sprintf('%.1f',max(qRange))})
    elseif max(qRange) - thresh(k) < 0.05
        set(gca, 'XTick',[min(qRange),thresh(k)])
        set(gca, 'XTickLabel',{sprintf('%.0f',min(qRange)),sprintf('%.2f',thresh(k))})
    else
        set(gca, 'XTick',[min(qRange),thresh(k),max(qRange)])
        set(gca, 'XTickLabel',{sprintf('%.0f',min(qRange)),sprintf('%.2f',thresh(k)),sprintf('%.1f',max(qRange))})
    end
    ylim([0,1]);
    if k == K
        set(gca, 'YTick',[0,1])
        set(gca,'YTickLabel',{'0','1'})
    else
        set(gca, 'YTick',[0,1])
        if isMulti
            set(gca,'YTickLabel',{'0','F^1'})
        else
            set(gca,'YTickLabel',{'0','F'})
        end
    end
    axis tight
    grid on
    set(gca,'FontSize',18)
end

[Vtmp,VE,VEK,VENK,VImi,VImiK,VImiNK,VIfa] = evalSys(cascade,thresh);
VI = VImi + VIfa;
figure; hold on; grid on;
plot(q,VE(:,1),'b-.','linewidth',3); plot(q,VImi(:,1)/cascade.CM,'r-','linewidth',3); plot(q,VIfa(:,1)/cascade.CA,'g--','linewidth',3);
if isMulti
    xlabel('\pi_0^1'); ylabel('Risk'); title(['\lambda = ' num2str(lambda)])
else
    xlabel('\pi_0'); ylabel('Risk'); title(['\lambda = ' num2str(lambda)])
end
legend('Weighted Resource Cost','Miss Rate','False-Alarm Rate','location','northwest');
set(gca,'FontSize',18)
%{
figure; hold on; plot(q,V2(:,1),'b');plot(q,VE(:,1)+VI(:,1),'r-.');
figure; hold on; plot(q,VI(:,1),'b');plot(q,VImi(:,1)+VIfa(:,1),'r-.');
V2 = evalSys(cascade,rand(1,K));
%}

% conditional activation probability at each stage
prob = condActProb(cascade, thresh);
figure; h = suptitle(['\lambda = ' num2str(lambda)]); set(h,'FontSize',18)
for k = 1:K
    subplot(K,1,k); plot(q,prob(:,k),'linewidth',5)
    if k == K
        if isMulti
            ylabel(sprintf('P(\\delta_%d^{1^\\ast} = 1 | \\pi_%d^1)',k,k-1))
            xlabel(sprintf('\\pi_%d^1',k-1));
        else
            ylabel(sprintf('P(\\delta_%d^\\ast = 1 | \\pi_%d)',k,k-1))
            xlabel(sprintf('\\pi_%d',k-1));
        end
    else
        if isMulti
            %ylabel(sprintf('P(\\pi_%d^1 \\geq \\tau_%d^{1\\ast} | \\pi_%d^1)',k,k,k-1))
            ylabel(sprintf('P(\\delta_%d^{1\\ast} = F^1 | \\pi_%d^1)',k,k-1))
            xlabel(sprintf('\\pi_%d^1',k-1));
        else
            %ylabel(sprintf('P(\\pi_%d \\geq \\tau_%d^\\ast | \\pi_%d)',k,k,k-1))
            ylabel(sprintf('P(\\delta_%d^\\ast = F | \\pi_%d)',k,k-1))
            xlabel(sprintf('\\pi_%d',k-1));
        end
    end
    axis tight
    set(gca,'FontSize',18)
end

% comparison with two stage
cascade2S.D = [84.36*0.016 1097*0.011+15131*0.014]*lambda/3.6;
cascade2S.accDO = cumsum([nan 264*0.014]*lambda/3.6, 2, 'reverse');
cascade2S.CM = cascade.CM;
cascade2S.CA = cascade.CA;
cascade2S.P0{1} = cascade.P0{1}; cascade2S.P0{2} = cascade.P0{3};
cascade2S.P1{1} = cascade.P1{1}; cascade2S.P1{2} = cascade.P1{3};
cascade2S.b = (0:1/(2e2-1):1); % dummy probability
cascade2S.eps0 = [0.1];
cascade2S.eps1 = [0.1];
cascade2S.nu0 = [0.1];
cascade2S.nu1 = [0.1];
cascade2S.initbL = 0.05;
cascade2S.initbU = 0.15;

[thresh2S,V2S] = optSys(cascade2S);
[Vtmp2S,VE2S,VEK2S,VENK2S,VImi2S,VImiK2S,VImiNK2S,VIfa2S] = evalSys(cascade2S,thresh2S);

% comparison with duty cycling
Ddc = 1097*0.011+15131*0.014;
ddc = 264*0.014;

piTarget = cascade.initbL:mean(diff(cascade.b)):cascade.initbU;

iROn = zeros(1,numel(piTarget));
iROff = zeros(1,numel(piTarget));
iEOn = zeros(1,numel(piTarget));
iEOff = zeros(1,numel(piTarget));
iPfaOn = zeros(1,numel(piTarget));
iPfaOff = zeros(1,numel(piTarget));
iPmiOn = zeros(1,numel(piTarget));
iPmiOff = zeros(1,numel(piTarget));

Rdc = zeros(1,numel(piTarget));
Edc = zeros(1,numel(piTarget));
Pfadc = zeros(1,numel(piTarget));
Pmidc = zeros(1,numel(piTarget));

R = zeros(1,numel(piTarget));
E = zeros(1,numel(piTarget));
Pfa = zeros(1,numel(piTarget));
Pmi = zeros(1,numel(piTarget));

R2S = zeros(1,numel(piTarget));
E2S = zeros(1,numel(piTarget));
Pfa2S = zeros(1,numel(piTarget));
Pmi2S = zeros(1,numel(piTarget));

k = 1;
for pi0 = piTarget
    % compare with the performance of the theoretically best dc
    idx0 = nearestIdx(q,pi0);
    
    % ideal dc
    iEOn(k) = cascade.D(end)/lambda*3.6;
    iEOff(k) = cascade.accDO(end)/lambda*3.6;
    iPfaOn(k) = VIfa(idx0,1)/cascade.CA;
    iPfaOff(k) = 0;
    iPmiOn(k) = VImiK(idx0,1)/cascade.CM;
    iPmiOff(k) = pi0;
    iROn(k) = iPfaOn(k)*cascade.CA + iPmiOn(k)*cascade.CM + iEOn(k)*lambda/3.6;
    iROff(k) = iPfaOff(k)*cascade.CA + iPmiOff(k)*cascade.CM + iEOff(k)*lambda/3.6;
    
    % compare with an energy-equivalent dc
    rho = (VE(idx0,1)/lambda*3.6 - ddc) / (Ddc - ddc);
    [Rfa,Rmi] = moduleRisks(cascade, thresh, 3, pi0);
    
    Edc(k) = rho*Ddc + (1-rho)*ddc;
    Pfadc(k) = rho*Rfa/cascade.CA;
    Pmidc(k) = rho*Rmi/cascade.CM + (1-rho)*pi0;
    Rdc(k) = Pfadc(k)*cascade.CA + Pmidc(k)*cascade.CM + Edc(k)*lambda/3.6;
    
    % guided processing
    E(k) = VE(idx0,1)/lambda*3.6;
    Pfa(k) = VIfa(idx0,1)/cascade.CA;
    Pmi(k) = VImi(idx0,1)/cascade.CM;
    R(k) = Pfa(k)*cascade.CA + Pmi(k)*cascade.CM + E(k)*lambda/3.6;
    
    % guided processing 2 stage
    E2S(k) = VE2S(idx0,1)/lambda*3.6;
    Pfa2S(k) = VIfa2S(idx0,1)/cascade2S.CA;
    Pmi2S(k) = VImi2S(idx0,1)/cascade2S.CM;
    R2S(k) = Pfa2S(k)*cascade2S.CA + Pmi2S(k)*cascade2S.CM + E2S(k)*lambda/3.6;
    
    k = k + 1;
end

figure; hold on; 
plot(piTarget,iROn,'r--','linewidth',3);
plot(piTarget,iROff,'b-.','linewidth',3);
plot(piTarget,Rdc,'g-','linewidth',3);
plot(piTarget,R2S,'m:d','linewidth',3);
plot(piTarget,R,'c-d','linewidth',3);
legend('dc ideal \rho=1','dc ideal \rho=0','dc real','gp 2-stage','gp')
xlabel('\pi_0'); ylabel('System risk (Energy-inefficiency)')
title(['\lambda = ' num2str(lambda)])
set(gca,'FontSize',18)

figure; hold on; 
plot(piTarget,iEOn,'r--','linewidth',3);
plot(piTarget,iEOff,'b-.','linewidth',3);
plot(piTarget,Edc,'g-','linewidth',3);
plot(piTarget,E2S,'m:d','linewidth',3);
plot(piTarget,E,'c-d','linewidth',3);
legend('dc ideal \rho=1','dc ideal \rho=0','dc real','gp 2-stage','gp')
xlabel('\pi_0'); ylabel('Energy consumption (mJ)')
title(['\lambda = ' num2str(lambda)])
set(gca,'FontSize',18)

figure; hold on; 
plot(piTarget,iPfaOn*100,'r--','linewidth',3);
plot(piTarget,iPfaOff*100,'b-.','linewidth',3);
plot(piTarget,Pfadc*100,'g-','linewidth',3);
plot(piTarget,Pfa2S*100,'m:d','linewidth',3);
plot(piTarget,Pfa*100,'c-d','linewidth',3);
legend('dc ideal \rho=1','dc ideal \rho=0','dc real','gp 2-stage','gp')
xlabel('\pi_0'); ylabel('False-alarm rate (%)')
title(['\lambda = ' num2str(lambda)])
set(gca,'FontSize',18)
%max(Pfadc./Pfa)

figure; hold on; 
plot(piTarget,iPmiOn*100,'r--','linewidth',3);
plot(piTarget,iPmiOff*100,'b-.','linewidth',3);
plot(piTarget,Pmidc*100,'g-','linewidth',3);
plot(piTarget,Pmi2S*100,'m:d','linewidth',3);
plot(piTarget,Pmi*100,'c-d','linewidth',3);
legend('dc ideal \rho=1','dc ideal \rho=0','dc real','gp 2-stage','gp')
xlabel('\pi_0'); ylabel('Miss rate (%)')
title(['\lambda = ' num2str(lambda)])
set(gca,'FontSize',18)
%max(Pmidc./Pmi)

%{
detector.P1 = cascade.P1{end};
detector.P0 = cascade.P0{end};
detector.b = (0:1/(1e3-1):1); % dummy probability
detector.CM = 1;
detector.CA = 1;
[thresh1,V1] = optBayes(detector);
%}

%% multiple application optimization
% Generate feature models
% app 1
lambda = 0.0043; % lower for higher energy saving ratio and vice versa
cascade.D = [24*0.016 250*0.011+1317.8*0.37e-6 1317.8*0.015]*lambda; %scaled down by 3.6 V
K = numel(cascade.D);
cascade.accDO = zeros(1,K);
cascade.CM = 2;
cascade.CA = 1;
[cascade.P0,cascade.P1] = getPMFs();
cascade.b = (0:1/(2e2-1):1); % dummy probability
cascade.eps0 = [0.1 0.1];
cascade.eps1 = [0.1 0.1];
%cascade.nu0 = kolmogorovDist(cascade.P0{k},cascade.P0{K});
cascade.nu0 = [0.1 0.1];
%cascade.nu1 = kolmogorovDist(cascade.P1{k},cascade.P1{K})/5;
cascade.nu1 = [0.1 0.1];
cascade.initbL = 0.05;
cascade.initbU = 0.20;
% app 2
cascade2 = cascade;
%{
lambda2 = lambda;
cascade.D = [1.8+24*0.5 250*0.05+1317.8*0.05 1317.8*0.5]*lambda2;
cascade2.accDO = [0 0 0];
cascade2.CM = 2;
cascade2.CA = 1;
[cascade2.P0,cascade2.P1] = getPMFs();
cascade2.b = (0:1/(2e2-1):1); % dummy probability
cascade2.eps0 = [0 0];
cascade2.eps1 = [0 0];
%cascade.nu0 = kolmogorovDist(cascade.P0{k},cascade.P0{K});
cascade2.nu0 = [0 0];
%cascade.nu1 = kolmogorovDist(cascade.P1{k},cascade.P1{K})/5;
cascade2.nu1 = [0.4 0.6];
cascade2.initbL = 0.1;
cascade2.initbU = 0.2;
%}

[thresh,V,thresh2,V2,VE2,VI2] = optMultiSys(cascade,cascade2);
%figure; surf(V2(:,:,1)-(VE2(:,:,1)+VI2(:,:,1)))

[B2,B] = meshgrid(cascade.b, cascade2.b);
figure;
for k = 1:K+1
    subplot(2,2,k); surf(B2,B,V2(:,:,k))
    shading interp;
    xlabel(sprintf('\\pi_%d^2',k-1));ylabel(sprintf('\\pi_%d^1',k-1));zlabel(sprintf('V_%d^2',k-1))
    set(gca,'FontSize',18)
end
figure;
for k = 1:K+1
    subplot(2,2,k); surf(B2,B,thresh2(:,:,k)-1)
    if k == K+1
        shading interp;
        set(gca, 'ZTick',[0,1])
    else
        shading interp; zlim([0,2]); caxis([0,2])
        set(gca, 'ZTick',[0,1,2])
        set(gca,'ZTickLabel',{'0','F^2','F^1'})
    end
    xlabel(sprintf('\\pi_%d^2',k-1));ylabel(sprintf('\\pi_%d^1',k-1));zlabel(sprintf('\\delta_%d^{2\\ast}',k-1))
    set(gca,'FontSize',18)
end
figure; hold on; grid on
hSurface1 = surf(B2,B,VE2(:,:,1));
hSurface2 = surf(B2,B,VI2(:,:,1));
set(hSurface1,'FaceColor',[0 1 0]);
set(hSurface2,'FaceColor',[1 0 0]);
xlabel(sprintf('\\pi_%d^2',0));ylabel(sprintf('\\pi_%d^1',0));zlabel(sprintf('Risk'))
title(['\lambda = ' num2str(lambda)])
set(gca,'FontSize',18)
shading interp;
axis tight;
legend('Weighted Resource Cost','Bayes Risk','location','northwest');

%(nanmean(VE(:,1))/lambda*3.6) / (nanmean(reshape(VE2(:,:,1),[numel(VE2(:,:,1)) 1]))/lambda*3.6)
%(nanmean(VI(:,1))/cascade.CM) / (nanmean(reshape(VI2(:,:,1),[numel(VI2(:,:,1)) 1]))/cascade.CM) % approximately all misses
%(nanmean(VE(:,1))/lambda*3.6) + (nanmean(reshape(VE2(:,:,1),[numel(VE2(:,:,1)) 1]))/lambda*3.6) + 3.6*0.032

%% comparison with exhausted search
M = 1e1;
p = (0:1/(M-1):1)';
VS = cell(M,M,M);
parfor k = 1:M
    for l = 1:M
        for m = 1:M
            VS{k,l,m} = evalSys(cascade,[p(k) p(l) p(m)]);
        end
    end
end

figure; hold on;
plot(q,V(:,1),'b','linewidth',5)
for k = 1:M
    for l = 1:M
        for m = 1:M
            plot(q,VS{k,l,m}(:,1))
        end
    end
end
plot(q,V(:,1),'b','linewidth',5)
if isMulti
    xlabel('\pi_0^1'); ylabel('System Risk'); title(['\lambda = ' num2str(lambda)])
else
    xlabel('\pi_0'); ylabel('System Risk'); title(['\lambda = ' num2str(lambda)])
end
legend('The optimal threshold','location','northwest')
set(gca,'FontSize',15)
