# Stage class 
#
# Long Le <longle1@illinois.edu>
# University of Illinois
#

import os,sys
sys.path.append(os.path.expanduser('~')+'/learnCS/graphSearch')
from GraphNode import GraphNode,visualize

import numpy as np

class StageNode(GraphNode):
    def __init__(self,D,d,CM,CA,Lambda,P0,P1,M):
        GraphNode.__init__(self,{})

        # hardcoded lambda
        self.val['lambda'] = Lambda
        # resource costs
        self.val['D'] = D
        self.val['d'] = d
        # miss and false alarm costs
        self.val['CM'] = CM
        self.val['CA'] = CA
        # likelihood models
        self.val['P0'] = P0
        self.val['P1'] = P1

        # belief points
        self.val['b'] = np.arange(0+1/(M-1),1-1/(M-1),1/(M-1))
        self.val['bL'] = 0.
        self.val['bH'] = 1.

        # value function/mapping
        self.val['V'] = {}
        self.val['E'] = {}
        self.val['Imk'] = {}
        self.val['ImK'] = {}
        self.val['IaK'] = {}
        for b in self.val['b']:
            if b > self.val['bL'] and b < self.val['bH']:
                self.val['V'][b] = np.infty
                self.val['E'][b] = np.infty
                self.val['Imk'][b] = np.infty
                self.val['ImK'][b] = np.infty
                self.val['IaK'][b] = np.infty

        # decision function/mapping
        self.val['dec'] = {}
        for b in self.val['b']:
            if b > self.val['bL'] and b < self.val['bH']:
                self.val['dec'][b] = -1

