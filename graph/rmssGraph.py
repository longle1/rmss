# Optimize graph-based sensing systems
#
# Long Le <longle1@illinois.edu>
# University of Illinois
#

import os,sys
sys.path.append(os.path.expanduser('~')+'/learnCS/graphSearch')
sys.path.append('../../learnCS/graphSearch')
from search import hasCycle2,allIn

import numpy as np
import matplotlib.pyplot as plt

def postTraverse(nodes,start,cb):
    # non-recursive/iterative implementation
    # post-order traversal
    # in-graph modification (no return values)
    if hasCycle2(nodes,start):
        print('cycle detected')
        return 

    explored = set()
    frontierS = []
    frontierS.append(start)
    while len(frontierS) > 0:
        node = frontierS[-1]
        #print('peek = '+str(node.val))

        if node in explored:
            frontierS.pop()
            continue
        if len(node.ngbs) == 0 or allIn(node.ngbs, explored):
            # call the state-less callback here
            cb(nodes,node)
            explored.add(node)
            frontierS.pop()
            #print('pop = '+str(node.val))

        # visit neighbors
        for ngb in node.ngbs:
            if ngb not in explored:
                frontierS.append(ngb)

    return

# The functions below are state-less callbacks
# (and their helpers) with the post-order 
# traversal assumption.
def dAcc(nodes,start):
    acc = -start.val['d']

    explored = set()
    frontierS = []
    frontierS.append(start)
    while len(frontierS) > 0:
        node = frontierS.pop()

        if node in explored:
            continue
        acc += node.val['d']
        explored.add(node)

        # visit neighbors
        for ngb in node.ngbs:
            if ngb not in explored:
                frontierS.append(ngb)

    start.val['dAcc'] = acc
    return

def valueFun(nodes,node):
    #objFun = {b:np.infty for b in node.val['b']}
    objFun = {}
    ergFun = {}
    ImkFun = {}
    ImKFun = {}
    IaKFun = {}
    for b in node.val['b']:
        if b > node.val['bL'] and b < node.val['bH']:
            if len(node.ngbs) == 0:
                # the last stage
                objFun[b] = [node.val['CM']*b,
                             node.val['CA']*(1-b)]
                ergFun[b] = [0,0]
                ImkFun[b] = [0,0]
                ImKFun[b] = [node.val['CM']*b,0]
                IaKFun[b] = [0,node.val['CA']*(1-b)]
            else:
                # the intermediate stage
                objFun[b] = [node.val['CM']*b+
                             node.val['lambda']*node.val['dAcc']]
                ergFun[b] = [node.val['lambda']*node.val['dAcc']]
                ImkFun[b] = [node.val['CM']*b]
                ImKFun[b] = [0]
                IaKFun[b] = [0]
                for ngb in node.ngbs:
                    objFun[b].append(ngb.val['lambda']*ngb.val['D']+
                                     expected(ngb,b))
                    ergFun[b].append(ngb.val['lambda']*ngb.val['D']+
                                     expected(ngb,b,key='E'))
                    ImkFun[b].append(expected(ngb,b,key='Imk'))
                    ImKFun[b].append(expected(ngb,b,key='ImK'))
                    IaKFun[b].append(expected(ngb,b,key='IaK'))

            node.val['V'][b] = np.min(objFun[b])
            node.val['dec'][b] = np.argmin(objFun[b])
            node.val['E'][b] = ergFun[b][node.val['dec'][b]]
            node.val['Imk'][b] = ImkFun[b][node.val['dec'][b]]
            node.val['ImK'][b] = ImKFun[b][node.val['dec'][b]]
            node.val['IaK'][b] = IaKFun[b][node.val['dec'][b]]

    return

def parents(nodes,node):
    # find parents of node
    node.val['parents'] = set()
    for ngb in node.ngbs:
        if node not in ngb.val['parents']:
            ngb.val['parents'].add(node)

    return

def actProb(nodes,node):
    N = len(node.val['P0'])

    bL = 1.0
    bH = 0.0
    for parent in node.val['parents']:
        bL = min(bL,parent.val['bL'])
        bH = max(bH,parent.val['bH'])

    q = {} # map dec,b to act prob
    for b in node.val['b']:
        if b > bL and b < bH:
            posts = posterior(node,b)
            evids = evidence(node,b)
            #print('np.nansum(evids) = %f' % np.nansum(evids))
            for k in range(N):
                dec = node.val['dec'].get(nearest(posts[k],node.val['b']),np.nan)
                if not np.isnan(dec):
                    if dec not in q:
                        q[dec] = {}
                    if b not in q[dec]:
                        q[dec][b] = 0
                    q[dec][b] += evids[k]

    #print('q = '+str(q))
    node.val['actProb'] = q

    return

def expected(node,prior,key='V'):
    N = len(node.val['P0'])
    V = np.zeros(N)
    posts = posterior(node,prior)
    evids = evidence(node,prior)
    for k in range(N):
        V[k] = node.val[key].get(nearest(posts[k],node.val['b']),np.nan)
    return np.nansum(evids*V)

def nearest(P,Ps):
    return min(Ps, key=lambda x:abs(x-P))

def posterior(node,prior):
    N = len(node.val['P0'])
    rv = np.zeros(N)
    for k in range(N):
        if prior == 0:
            rv[k] = 0.
        else:
            rv[k] = 1/(1+(1-prior)/prior/(node.val['P1'][k]/node.val['P0'][k]))
    return rv

def evidence(node,prior):
    N = len(node.val['P0'])
    rv = np.zeros(N)
    for k in range(N):
        rv[k] = node.val['P0'][k]*(1-prior)+node.val['P1'][k]*prior
    return rv

