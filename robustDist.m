function [rp0,rp1] = robustDist(p0,p1,params)
% [rp0,rp1] = robustDist(p0,p1,params)
% Robustify the nomial distribution p0 and p1
%
% Long Le
% University of Illinois
%

% distance to the optimal/best stage
nu0 = params.nu0;
nu1 = params.nu1;
eps0 = params.eps0;
eps1 = params.eps1;

v = (eps1+nu1)/(1-eps1);
vv = (eps0+nu0)/(1-eps0);
w = (nu0)/(1-eps0);
ww = (nu1)/(1-eps1);

% find the least-favorable density
%L = p1./p0;F0 = cumsum(p0); F1 = cumsum(p1); % applicable only when likelihood is a monotone function
[F0,F1,L] = likCumsum(p0,p1);
% find yL
fz = (L.*F0 - F1) - (v + w*L);
%figure; plot(fz(fz<100),'x-'); grid on
[~,idx] = min(abs(fz));
iL = idx;
% find yH
fz = (1 - F1) - L.*(1-F0) - (ww + vv*L);
%figure; plot(fz(fz>-100),'x-'); grid on
[~,idx] = min(abs(fz));
iU = idx;
% find a0L,b0L,a0H,b0H,a1L,b1L,a1H,b1H
a0L = (1-eps0)/(v+w*L(iL))*v;
b0L = (1-eps0)/(v+w*L(iL))*w;
a0U = (1-eps0)/(ww+vv*L(iU))*ww;
b0U = (1-eps0)/(ww+vv*L(iU))*vv;
a1L = (1-eps1)*L(iL)/(v+w*L(iL))*v;
b1L = (1-eps1)*L(iL)/(v+w*L(iL))*w;
a1U = (1-eps1)*L(iU)/(ww+vv*L(iU))*ww;
b1U = (1-eps1)*L(iU)/(ww+vv*L(iU))*vv;

% modify the observation density
l = p1./p0;

rp0 = zeros(size(p0));
rp0(l<L(iL)) = a0L*p0(l<L(iL)) + b0L*p1(l<L(iL));
rp0(l>=L(iL) & l<=L(iU)) = (1-eps0)*p0(l>=L(iL) & l<=L(iU));
rp0(l>L(iU)) = a0U*p0(l>L(iU)) + b0U*p1(l>L(iU));

rp1 = zeros(size(p1));
rp1(l<L(iL)) = a1L*p0(l<L(iL)) + b1L*p1(l<L(iL));
rp1(l>=L(iL) & l<=L(iU)) = (1-eps1)*p1(l>=L(iL) & l<=L(iU));
rp1(l>L(iU)) = a1U*p0(l>L(iU)) + b1U*p1(l>L(iU));

%{
figure; 
subplot(211); hold on; plot(rp0,'b-'); plot(p0,'r:'); ylabel('p0')
subplot(212); hold on; plot(rp1,'b-'); plot(p1,'r:'); ylabel('p1')
figure; hold on; plot(rp1./rp0,'b-'); plot(p1./p0,'r:'); ylabel('p1./p0')
%}