function [belief,prob] = stateObsProb(cascade,k,l,m)
% [belief,prob] = stateObsProb(cascade,k,l,p,m)
% posterior update + expectation probability
%
% Long Le <longle1@illinois.edu>
% University of Illinois
% 
    prob = cascade.P1{k}(l)*cascade.b(m) + cascade.P0{k}(l)*(1-cascade.b(m));
    belief = cascade.P1{k}(l)*cascade.b(m)/prob;
end