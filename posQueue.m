classdef posQueue
    properties
        buf = zeros(0,2);
        cnt = 0;
    end
    methods
        function obj = enqueue(obj,pos)
            obj.cnt = obj.cnt + 1;
            obj.buf(obj.cnt,:) = pos;
        end
        function [obj,r] = dequeue(obj)
            r = obj.buf(1,:);
            obj.buf(1,:) = [];
            obj.cnt = obj.cnt-1;
        end
        function r = isEmpty(obj)
            r = obj.cnt == 0;
        end
    end
end