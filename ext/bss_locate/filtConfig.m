function [J,RT,d,r,k] = filtConfig(fname)
% [J,RT,d,r,k] = filtConfig(fname)
% Return filter configuration
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

% <J>src_<RT>_<d>_<r>_config<k>.mat
while 1
    arr = sscanf(fname,'%dsrc_%dms_%dm_%dm_config%d');
    if numel(arr) == 5
        d = arr(3);
        r = arr(4);
        break;
    end
    
    arr = sscanf(fname,'%dsrc_%dms_%dcm_%dm_config%d');
    if numel(arr) == 5
        d = arr(3)/1e2;
        r = arr(4);
        break;
    end

    arr = sscanf(fname,'%dsrc_%dms_%dm_%dcm_config%d');
    if numel(arr) == 5
        d = arr(3);
        r = arr(4)/1e2;
        break;
    end
    
    arr = sscanf(fname,'%dsrc_%dms_%dcm_%dcm_config%d');
    if numel(arr) == 5
        d = arr(3)/1e2;
        r = arr(4)/1e2;
        break;
    end
end
J = arr(1);
RT = arr(2)/1e3;
k = arr(5);