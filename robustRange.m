function [piL,piU] = robustRange(cascade,k,piLIn,piUIn)
% [piL,piU] = robustRange(cascade)
%
% Long le
% University of Illinois
%

% find belief range
p0 = cascade.P0{k};
p1 = cascade.P1{k};
lL = min(p1./p0);
lU = max(p1./p0);
piL = 1/(1 + (1-piLIn)/piLIn/lL);
piU = 1/(1 + (1-piUIn)/piUIn/lU);

piL = nearestVal(cascade.b,piL);
piU = nearestVal(cascade.b,piU);