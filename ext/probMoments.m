function [mu,K] = probMoments(p,x)
% [m,K] = probMoments(p,x)
% Compute 1st and 2nd moments of a probability
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

mu = sum(p.*x);
K = sum(p.*(x-mu).^2);