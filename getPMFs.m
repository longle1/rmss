function [pmf0,pmf1] = getPMFs()
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
K = 3;
pmf0 = cell(K,1);
pmf1 = cell(K,1);
%{
rng(1);
%SNR = [1.5 1.3 3.0];% 1 app
SNR = [1.6 1.4 3.0];% 2 apps
yRange = [-3:0.5:6];
n1 = zeros(numel(yRange),K);
x1 = zeros(numel(yRange),K);
n0 = zeros(numel(yRange),K);
x0 = zeros(numel(yRange),K);
for k = 1:K
    y1 = randn(1,1e4)+SNR(k);
    y0 = randn(1,1e4);
    [n1(:,k),x1(:,k)] = hist(y1,yRange);
    [n0(:,k),x0(:,k)] = hist(y0,yRange);
    %figure; bar(union(x0(:,k),x1(:,k)),[n0(:,k)/sum(n0(:,k)) n1(:,k)/sum(n1(:,k))]);
    n1(n1==0) = eps;
    n0(n0==0) = eps;
    pmf1{k} = n1(:,k)'/sum(n1(:,k));
    pmf0{k} = n0(:,k)'/sum(n0(:,k));
end
%}

load('localLogs/label.mat','label');
for k = 1:K
    load(['localLogs/det' +num2str(k) '.mat'],['det' num2str(k)]);
    %load(['localLogs/det' +num2str(1) '.mat'],['det' num2str(1)]);
    
    det0 = eval(['det' +num2str(k) '(label==0)']);
    %det0 = eval(['det' +num2str(1) '(label==0)']);
    [F0,x0] = ecdf(det0);
    [p0,x0] = cdf2pmf(F0,x0);
    
    det1 = eval(['det' +num2str(k) '(label==1)']);
    %det1 = eval(['det' +num2str(1) '(label==1)']);
    [F1,x1] = ecdf(det1);
    [p1,x1] = cdf2pmf(F1,x1);
    
    X = linspace(min(min(x0),min(x1)),max(max(x0),max(x1)),1e2)';
    pmf0{k} = probVecQuan(x0,p0,X);
    pmf1{k} = probVecQuan(x1,p1,X);

    %{
    figure; hold on;
    plot(x0,F0); plot(x1,F1);
    %}
end
