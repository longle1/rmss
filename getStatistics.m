% get detector statistics
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;
addpath('C:/cygwin64/home/BLISSBOX/voicebox/');
addpath('C:/cygwin64/home/BLISSBOX/audio_class/matlab/');
%addpath('C:/cygwin64/home/UyenBui/voicebox/');
%addpath('C:/cygwin64/home/UyenBui/audio_class/matlab/');
%addpath('C:/Users/Long/Projects/voicebox/');
%addpath('C:/Users/Long/Projects/audio_class/matlab/');
%addpath('/home/blissbox/voicebox/');
%addpath('/home/blissbox/audio_class/matlab/');

%% Det 1
%[y,fs] = audioread('../audio_class/realdata/train.wav');
%fid = fopen('../audio_class/realdata/Label Track GCWA.txt');
[y,fs] = audioread('./data/test.wav');
fid = fopen('./data/Label Track Y.txt');
C = textscan(fid,'%f %f %s');
fclose(fid);

nWin = 512;
nInc = nWin/2;
%Y = enframe(y,nWin,nInc);
[newState,specDet,speclog_prob_noise_ridge_cum] = detectOneFile([],y,fs,nWin,nInc);

t1 = C{1};
t2 = C{2};
label = zeros(1,size(specDet,2));
for k = 1:numel(t1)
    label(round(t1(k)*fs/nInc):round(t2(k)*fs/nInc)) = 1;
end
label = logical(label);

det1_max = max(specDet,[],1);
[pd_max, pf_max, thres_max] = roc(label,det1_max);

det1_mean = mean(specDet,1);
[pd_mean, pf_mean, thres_mean] = roc(label,det1_mean);

det1 = det1_mean;
[prec1,tpr1,fpr1,thresh1] = prec_rec(det1,label);

figure;
ax(1) = subplot(211);imagesc(specDet); axis xy
ax(2) = subplot(212);plot(label); axis xy
linkaxes(ax,'x')
axis xy

%{
det1_mean = mean(speclog_prob_noise_ridge_cum,1);
det1 = det1_mean;
det1_mean = mean(specDet,1);
det1sup = det1_mean;
figure; hold on;
[prec1,tpr1,fpr1,thresh1] = prec_rec(det1,label);
plot(tpr1,prec1)
[prec1,tpr1,fpr1,thresh1] = prec_rec(det1sup,label);
plot(tpr1,prec1)
legend('full','suppressed')
%}

%% Det 2
h1 = fir1(128,[4500,6500]/(fs/2));
H1 = abs(freqz(h1,1,256));
h2 = fir1(128,[7000,8000]/(fs/2));
H2 = abs(freqz(h2,1,256));
tic
det2 = max([H1'*specDet;H2'*specDet],[],1);
runTime2 = toc;

[pd2, pf2, thres2] = roc(label,det2);
[prec2,tpr2,fpr2,thresh2] = prec_rec(det2,label);

figure; hold on;plot(H1);plot(H2)

%% Det 3
% test templates and the template matching alg
idx = 8;
temp = specDet(:,round(t1(idx)*fs/nInc):round(t2(idx)*fs/nInc));

idx = 11;
data = specDet(:,round(t1(idx)*fs/nInc):round(t2(idx)*fs/nInc));
%data = specDet(:,round(t2(idx-1)*fs/nInc):round(t1(idx)*fs/nInc));data = data(:,1:150);
%data = specDet(:,26232:26252);
%[yi,fs] = audioread('./data/jackhammer_1.wav'); yi = resample(yi,24000,fs); fs = 24000;
%[~,data,~] = detectOneFile([],yi,fs,nWin,nInc);
tic
[score,allPos,C,V,lnk] = tempMatch(temp,data,1);
runTime3 = toc;

det3 = zeros(1,size(data,2));
for k = 1:size(allPos,1)
    if C(allPos(k,1),allPos(k,2)) > 0
        det3(allPos(k,2)) = score;
    end
end

figure;
subplot(221); imagesc(temp); axis xy; colorbar;
subplot(222); imagesc(data); axis xy; colorbar;

subplot(223); hold on;
imagesc(C); axis tight; colorbar;
plot(allPos(:,2),allPos(:,1),'r')
title(['score = ' num2str(score)])
    
subplot(224);
plot(det3)

%% running through the data in realtime
win = 200;
inc = 50;

tic
sIdx = 1;%44000;
det3 = zeros(1,size(specDet,2));
lastScore = 0;
maxAllPos = nan;
maxC = nan;
while sIdx+win <= size(specDet,2)%45300
    data = specDet(:,sIdx:sIdx+win);
    data = max(bsxfun(@times,data,H1),bsxfun(@times,data,H2));
    [score,allPos,C,V,lnk] = tempMatch(temp,data,1);
    fprintf(1,'sIdx = %d, score = %.2f\n',sIdx,score);
    
    if score > lastScore
        maxAllPos = allPos;
        maxC = C;
    else
        if ~isnan(maxAllPos)
            fprintf(1,'\tlogged\n');
            for k = 1:size(maxAllPos,1)
                if maxC(maxAllPos(k,1),maxAllPos(k,2)) > 0
                    % log for the last block of frames
                    det3(sIdx-inc+maxAllPos(k,2)-1) = lastScore;
                end
            end
            % reset the allPos and C
            maxAllPos = nan;
            maxC = nan;
        end
    end
    lastScore = score;
    sIdx = min(size(specDet,2),sIdx+inc);
end
runTime = toc;

[pd3, pf3, thres3] = roc(label,det3);
[prec3,tpr3,fpr3,thresh3] = prec_rec(det3,label);

figure;
ax(1) = subplot(211); plot(det3); axis tight
ax(2) = subplot(212); imagesc(specDet); axis xy
linkaxes(ax,'x')

figure; 
subplot(121); hold on; grid on;
plot(fpr1,tpr1,'--b','linewidth',3)
plot(fpr2,tpr2,'-.r','linewidth',3)
plot(fpr3,tpr3,'-g','linewidth',3)
xlabel('False alarm rate');
ylabel('True detection rate');
legend(['Energy AUC=' num2str(trapz(fpr1,tpr1))],['Spectral AUC=' num2str(trapz(fpr2,tpr2))],['Temporal-Spectral AUC=' num2str(trapz(fpr3,tpr3))],'location','southeast')
title('ROC')
set(gca,'fontsize',18)

subplot(122); hold on; grid on;
plot(tpr1,prec1,'--b','linewidth',3)
plot(tpr2,prec2,'-.r','linewidth',3)
plot(tpr3,prec3,'-g','linewidth',3)
xlabel('Recall');
ylabel('Precision');
legend(['Energy AUC=' num2str(trapz(tpr1,prec1))],['Spectral AUC=' num2str(trapz(tpr2,prec2))],['Temporal-Spectral AUC=' num2str(trapz(tpr3,prec3))],'location','southwest')
title('PR')
set(gca,'fontsize',18)

figure; hold on; plot(thresh1, tpr1); plot(thresh1, prec1)
xlabel('Threshold')
legend('recall','precision')