function p = angSpec2prob(angSpec)
% p = angSpec2prob(angSpec)
% Convert angular spectrum to prob
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
x = angSpec - min(angSpec);
x(x==0) = eps;
p = x./sum(x);