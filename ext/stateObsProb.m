function [b,p] = stateObsProb(cascade,k,l,m)
% [b,p] = stateObsProb(cascade,k,l,m)
% Compute state prob + observation prob
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%
N = size(cascade.b,1);
q = zeros(N,1);
for n = 1:N
    q(n) = cascade.PY{n,k}(l)*cascade.b(n,m);
end
p = sum(q);
b = q/p;
