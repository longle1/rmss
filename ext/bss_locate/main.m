% Test bss_locate
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; close all;

addpath('filters');
addpath('sources');
addpath('../');

%% Get all observations and labels
files = dir('filters');
stype = 'male';
X = [-90:90];
methods = {'GCC-PHAT', 'GCC-NONLIN', 'MUSIC', 'DS', 'MVDR', 'DNM', 'DSW','MVDRW'};
allDOA = cell(numel(files)-2,numel(methods));
allDOAe = cell(numel(files)-2,numel(methods));
allSpec = zeros(numel(files)-2,numel(X),numel(methods));
allET = zeros(1,numel(methods)); % elapsed time
for m = 1:numel(methods)
    fprintf(1,'method: %s\n',methods{m});
    
    tic
    parfor l = 3:numel(files)
        fprintf(1,'at iter %d, filename: %s\n',l, files(l).name);

        fname = files(l).name;
        [J,RT,d,r,k] = filtConfig(fname);
        x = mix(fname,stype,[1 eps*ones(1,J-1)])'; % use only 1 source
        fs = 16e3;

        tau = true_tdoas(fname);
        [taue,spec] = bss_locate_spec(x,fs,d,J,methods{m});
        allDOA{l-2,m} = round(tau*343/d*90);
        allDOAe{l-2,m} = round(taue*343/d*90);
        allSpec(l-2,:,m) = spec;

        %{
        figure;
        plot(X,spec);
        title(['DOA: ' sprintf('%.2f^o ',tau*343/d*90)])
        [R, P, F, Acc] = eval_tdoas(taue, tau, d, 5);
        %}
    end
    allET(m) = toc;
end
save(sprintf('../localLogs/inputData_%s.mat',stype),'allSpec','allDOA','allDOAe','allET');
