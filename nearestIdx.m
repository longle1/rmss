function idx = nearestIdx(p,pVal)
% idx = nearestIdx(p,pVal)
%
% Long Le
% University of Illinois
%
    err = abs(pVal-p(:));
    [~,idx] = min(err);
end
