function prob = condActProb(cascade, thresh)
% prob = condActProb(cascade, thresh)
%
% Calculate activation probability
% VE is N-by-(1+K)
%
% Long Le
% University of Illinois
%

K = numel(cascade.D); % number of stages
M = size(cascade.b,2); % number of state discretization

% convert into robust observation densities
[cascade] = robustify(cascade);
piL = [cascade.initbL zeros(1,K-1)];
piU = [cascade.initbU zeros(1,K-1)];
for k = 1:K-1
    [piL(k+1),piU(k+1)] = robustRange(cascade,k,piL(k),piU(k));
end

prob = nan(M, K);
for k = 0:K-1
    L = size(cascade.P0{k+1},2); % size of the observation set
    for m = 1:M
        if cascade.b(m)>=piL(k+1) && cascade.b(m)<=piU(k+1)
            bb = zeros(L,1);
            pp = zeros(L,1);
            for l = 1:L
                [bb(l),pp(l)] = stateObsProb(cascade,k+1,l,m);
                bb(l) = nearestVal(cascade.b,bb(l));
            end
            prob(m,k+1) = sum(pp(bb >= thresh(k+1)))/sum(pp); % additional normalization to ensure sum-to-1
        end
    end
end