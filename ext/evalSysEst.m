function [V,VE,VI] = evalSysEst(cascade,thresh)
% [V,VE,VI] = evalSysEst(cascade,thresh)
% Evaluate an estimator-cascade for a given threshold
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

K = numel(cascade.D); % number of stages
L = size(cascade.PY{1,1},1); % size of the observation set
%D = size(cascade.PY{1,1},2); % observation dimension
%N = size(cascade.b,1); % number of states
%N = numel(cascade.X); % number of states
M = size(cascade.b,2); % number of belief points

V = zeros(M,K);
VE = zeros(M,K); % value functions of energy
VI = zeros(M,K); % value functions of inference
Cov = zeros(M,1);
for m = 1:M
    [~,Cov(m)] = probMoments(cascade.b(:,m),cascade.X);
    V(m,K) = Cov(m);
    VE(m,K) = 0;
    VI(m,K) = Cov(m);
end
for k = K-1:-1:1
    FR = zeros(M,L); % future reward
    FRE = zeros(M,L); % future reward
    FRI = zeros(M,L); % future reward
    parfor m = 1:M
        for l = 1:L
            [bb,pp] = stateObsProb(cascade,k+1,l,m);
            d = zeros(M,1);
            for mm = 1:M
                d(mm) = norm(cascade.b(:,mm)-bb,1);
            end
            [~,idx] = min(d);
            FR(m,l) = pp*V(idx,k+1);
            FRE(m,l) = pp*VE(idx,k+1);
            FRI(m,l) = pp*VI(idx,k+1);
        end
    end
    
    V(thresh(:,k)==0,k) = Cov(thresh(:,k)==0);
    V(thresh(:,k)==1,k) = cascade.D(k+1)+sum(FR(thresh(:,k)==1,:),2);
    VE(thresh(:,k)==0,k) = 0;
    VE(thresh(:,k)==1,k) = cascade.D(k+1)+sum(FRE(thresh(:,k)==1,:),2);
    VI(thresh(:,k)==0,k) = Cov(thresh(:,k)==0);
    VI(thresh(:,k)==1,k) = sum(FRI(thresh(:,k)==1,:),2);
end
% energy cost of the first stage
V(:,1) = cascade.D(1)+V(:,1);
VE(:,1) = cascade.D(1)+VE(:,1);