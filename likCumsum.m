function [lF0,lF1,sL] = likCumsum(p0,p1)
% [lF0,lF1,sL] = likCumsum(p0,p1)
% compute the cumsum of the likelihood function under both H0 and H1
%
% Long Le
% University of Illinois
%

L = p1./p0;
sL = sort(L);
lp0 = zeros(1,numel(sL));
lp1 = zeros(1,numel(sL));
for k = 1:numel(sL)
    lp0(k) = sum(p0(p1./p0 ==sL(k)));
    lp1(k) = sum(p1(p1./p0 ==sL(k)));
end
lF0 = cumsum(lp0);
lF1 = cumsum(lp1);
