function [Cx,f]=qstft(x,fs,wlen,lf,lt)

% QSTFT Quadratic linear-scale time-frequency transform based on the local
% covariance of a STFT with sine windows
%
% [Cx,f]=qstft(x,fs,wlen)
%
% Inputs:
% x: nsampl x nchan vector containing a multichannel signal
% fs: sampling frequency in Hz
% wlen: length of the STFT window (must be a power of 2)
% lf: half-width of the frequency neighborhood for the computation of
% empirical covariance
% lt: half-width of the time neighborhood for the computation of empirical
% covariance
%
% Output:
% Cx: nchan x nchan x nbin x nfram matrix containing the spatial covariance
% matrices of the input signal in all time-frequency bins
% f: nbin x 1 vector containing the center frequency of each frequency bin
% in Hz
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2008-2011 Emmanuel Vincent
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
[nsampl,nchan]=size(x);
if nchan>nsampl, error('The input signal must be in columns.'); end
if nargin<4, lf=2; end
if nargin<5, lt=2; end

%%% STFT %%%
X=stft_multi(x.',wlen);
[nbin,nfram,nchan]=size(X);

%%% Computation of local covariances %%%
winf=hanning(2*lf-1);
wint=hanning(2*lt-1).';
Cx=zeros(nchan,nchan,nbin,nfram);
for f=1:nbin,
    for t=1:nfram,
        indf=max(1,f-lf+1):min(nbin,f+lf-1);
        indt=max(1,t-lt+1):min(nfram,t+lt-1);
        nind=length(indf)*length(indt);
        XX=reshape(X(indf,indt,:),nind,nchan).';
        wei=reshape(winf(indf-f+lf)*wint(indt-t+lt),1,nind);
        Cx(:,:,f,t)=(XX.*(ones(nchan,1)*wei))*XX'/sum(wei);
    end
end
f=fs/wlen*(0:nbin-1).';

return;