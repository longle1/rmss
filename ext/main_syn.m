% main script for cascade estimation with synthetic data
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

clear all; %close all

%%
rng(1);
K = 3;
N = 3;
muN = [0 0.5 1];
sclK = [1 2 3];
yRange = [-3:0.5:6];
cascade.PY = cell(N,K);
for k = 1:K
    for n = 1:N
        y = randn(1,100)+sclK(k)*muN(n);
        nn = hist(y,yRange);
        nn = nn/sum(nn);
        nn(nn == 0) = eps;
        cascade.PY{n,k} = nn(:);
    end
end
cascade.X = [1 2 3]';

lambda = 0.001;
cascade.D = [1 10 100]*lambda;

%cascade.b = drchrnd([1 1 1], 1e2)';
m = 2; % dim-1
n = 100; % # of subintervals
M = simplex_grid_size(m,n);
mGrid = simplex_grid_index_all (m,n,M);
cascade.b = mGrid./n;

cascade.eps0 = [0 0];
cascade.eps1 = [0 0];
cascade.nu0 = [0 0];
cascade.nu1 = [0.05 0.50];

%% find optimal thresholds
[C,V] = optSysEst(cascade);
[Vtmp,VE,VI] = evalSysEst(cascade,C);
%norm(V-Vtmp)

% an opposite censoring region
C2 = zeros(size(C));
C2(C==1) = 0;
C2(C==0) = 1;
[V2,VE2,VI2] = evalSysEst(cascade,C2);

% a heuristic censoring region
sig = zeros(size(cascade.b,2),1);
for m = 1:size(cascade.b,2)
    [~,sig(m)] = probMoments(cascade.b(:,m),cascade.X);
end
%{
C3 = zeros(size(C));
allSig = min(sig):range(sig)/9:max(sig);
allV3 = cell(numel(allSig), numel(allSig));
allMeanV3 = zeros(numel(allSig), numel(allSig));
for k = 1:numel(allSig)
    for l = 1:numel(allSig)
        C3(:,1) = sig > allSig(k);
        C3(:,2) = sig > allSig(l);
        V3 = evalSysEst(cascade,C3);
        allV3{k,l} = V3;
        allMeanV3(k,l) = mean(V3(:,1));
    end
end
[r,c] = ind2sub(size(allMeanV3),find(allMeanV3==min(allMeanV3(:))));
C3(:,1) = sig > allSig(r(1));
C3(:,2) = sig > allSig(c(1));
[V3,VE3,VI3] = evalSysEst(cascade,C3);
%}

C3 = zeros(size(C));
C3(:,1) = sig > (min(sig)+max(sig))*0.75;
C3(:,2) = sig > (min(sig)+max(sig))*0.25;
[V3,VE3,VI3] = evalSysEst(cascade,C3);

C4 = zeros(size(C));
C4(:,1) = sig > (min(sig)+max(sig))*0.25;
C4(:,2) = sig > (min(sig)+max(sig))*0.75;
[V4,VE4,VI4] = evalSysEst(cascade,C4);

C5 = zeros(size(C));
C5(:,1) = sig > (min(sig)+max(sig))*0.75;
C5(:,2) = sig > (min(sig)+max(sig))*0.75;
[V5,VE5,VI5] = evalSysEst(cascade,C5);

C6 = zeros(size(C));
C6(:,1) = sig > (min(sig)+max(sig))*0.25;
C6(:,2) = sig > (min(sig)+max(sig))*0.25;
[V6,VE6,VI6] = evalSysEst(cascade,C6);

% save and plot
save('localLogs/main_sys.mat')

sysPlotC(cascade,C);
sysPlotV(cascade,V);
sysPlotV(cascade,VI);zlabel('MSE')
sysPlotV(cascade,VE);zlabel('Weighted Resource Cost')

sysPlotC(cascade,C2);
sysPlotV(cascade,V2);
sysPlotV(cascade,VI2);zlabel('MSE')
sysPlotV(cascade,VE2);zlabel('Weighted Resource Cost')

sysPlotC(cascade,C3);
sysPlotV(cascade,V3);
sysPlotV(cascade,VI3);zlabel('MSE')
sysPlotV(cascade,VE3);zlabel('Weighted Resource Cost')

sysPlotC(cascade,C4);
sysPlotV(cascade,V4);
sysPlotV(cascade,VI4);zlabel('MSE')
sysPlotV(cascade,VE4);zlabel('Weighted Resource Cost')

sysPlotC(cascade,C5);
sysPlotV(cascade,V5);
sysPlotV(cascade,VI5);zlabel('MSE')
sysPlotV(cascade,VE5);zlabel('Weighted Resource Cost')

sysPlotC(cascade,C6);
sysPlotV(cascade,V6);
sysPlotV(cascade,VI6);zlabel('MSE')
sysPlotV(cascade,VE6);zlabel('Weighted Resource Cost')
