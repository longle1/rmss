function P = probVecQuan(x,p,X)
% P = probVecQuan(x,p,X)
% C: centroid vector
% x: variable vector
% p: pdf vector
%
% P: pmf vector
% Long Le <longle1@illinois.edu>
% University of Illinois
%

idx = zeros(1,size(x,1));
for k = 1:size(x,1)
    [~,idx(k)] = min(abs(x(k)-X));
end

P = zeros(1,size(X,1));
for k = 1:size(X,1)
    P(k) = sum(p(idx == k));
end
% ensure non-zero pmf
P(P==0) = eps;
P = P./sum(P);