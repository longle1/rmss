function [mM,mMIdx] = matEleMin(varargin)
% [mM,mMIdx] = matEleMin(varargin)
% Compute matrix element-wise minimal
%
% Long Le
% University of Illinois
%

M = zeros(size(varargin{1},1),size(varargin{1},2),nargin);
for k = 1:nargin
    M(:,:,k) = varargin{k};
end

mM = zeros(size(M,1),size(M,2));
mMIdx = zeros(size(M,1),size(M,2));
for k = 1:size(M,1)
    for l = 1:size(M,2)
        [mM(k,l),mMIdx(k,l)] = min(M(k,l,:),[],'includenan');
    end
end