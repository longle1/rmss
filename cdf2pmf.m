function [pmf,xp] = cdf2pmf(F,x)
% [pmf,xp] = cdf2pmf(F,x)
% 
% Long Le <longle1@illinois.edu>
% University of Illinois
%

xp = zeros(0,1);
pmf = zeros(0,1);
k = 1;
kp = 1;
while k < numel(x)
    xp(kp,1) = x(k);
    if x(k+1) == x(k) % point mass
        pmf(kp,1) = F(k+2)-F(k);
        k = k+2;
    else
        pmf(kp,1) = F(k+1)-F(k); 
        k = k+1;
    end
    kp = kp+1;
end