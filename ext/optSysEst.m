function [C,V] = optSysEst(cascade)
% [thresh,V] = optSysEst(cascade)
% Compute optimal configuration for an estimator-cascade
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

K = numel(cascade.D); % number of stages
L = size(cascade.PY{1,1},1); % size of the observation set
%D = size(cascade.PY{1,1},2); % observation dimension
%N = size(cascade.b,1); % number of states
M = size(cascade.b,2); % number of belief points

V = zeros(M,K);
Cov = zeros(M,1);
for m = 1:M
    [~,Cov(m)] = probMoments(cascade.b(:,m),cascade.X);
    V(m,K) = Cov(m);
end
C = zeros(M,K-1);
for k = K-1:-1:1
    J = zeros(M,L);
    parfor m = 1:M
        for l = 1:L
            [bb,pp] = stateObsProb(cascade,k+1,l,m);
            d = zeros(M,1);
            for mm = 1:M
                d(mm) = norm(cascade.b(:,mm)-bb,1);
            end
            [~,idx] = min(d);
            J(m,l) = pp*V(idx,k+1);
        end
    end
    % check concavity
    %{
    tmp2 = sum(J,2);
    pIdx = 123; qIdx = 112;
    lambda = 0.5;
    p = cascade.b(:,pIdx);
    q = cascade.b(:,qIdx);
    d = zeros(size(cascade.b,2),1);
    for k = 1:numel(d)
        d(k) = norm(lambda*p+(1-lambda)*q - cascade.b(:,k),1);
    end
    [~,idx] = min(d);
    lambda*tmp2(pIdx) + (1-lambda)*tmp2(qIdx)
    tmp2(idx)
    %}
    C(:,k) = Cov >= cascade.D(k+1)+sum(J,2);
    
    V(:,k) = min(Cov,cascade.D(k+1)+sum(J,2));
end
% energy cost of the first stage
V(:,1) = cascade.D(1)+V(:,1);
