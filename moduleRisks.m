function [Rfa,Rmi] = moduleRisks(cascade, thresh, k, prior)
% [Rfa,Rmi] = moduleRisk(cascade, k, thresh, prior)
%
% Calculate error risk at stage k for a given prior
%
% Long Le
% University of Illinois
%

% convert into robust observation densities
[cascade] = robustify(cascade);

L = size(cascade.P0{k},2); % size of the observation set
bb = zeros(L,1);
for l = 1:L
    m = nearestIdx(cascade.b,prior);
    [bb(l),~] = stateObsProb(cascade,k,l,m);
    bb(l) = nearestVal(cascade.b,bb(l));
end
Rfa = cascade.CA*sum(cascade.P0{k}(bb >= thresh(k)))/sum(cascade.P0{k}); % additional normalization to ensure sum-to-1
Rmi = cascade.CM*sum(cascade.P1{k}(bb < thresh(k)))/sum(cascade.P1{k});
