% Title:    A LaTeX Template For Responses To a Referees' Reports
% Author:   Petr Zemek <s3rvac@gmail.com>
% Homepage: https://blog.petrzemek.net/2016/07/17/latex-template-for-responses-to-referees-reports/
% License:  CC BY 4.0 (https://creativecommons.org/licenses/by/4.0/)
\documentclass[10pt]{article}

% Allow Unicode input (alternatively, you can use XeLaTeX or LuaLaTeX)
\usepackage[utf8]{inputenc}

\usepackage{microtype,xparse,tcolorbox,url}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amssymb}
\newenvironment{reviewer-comment }{}{}
\tcbuselibrary{skins}
\tcolorboxenvironment{reviewer-comment }{empty,
  left = 1em, top = 1ex, bottom = 1ex,
  borderline west = {2pt} {0pt} {black!20},
}
\ExplSyntaxOn
\NewDocumentEnvironment {response} { +m O{black!20} } {
  \IfValueT {#1} {
    \begin{reviewer-comment~}
      \setlength\parindent{2em}
      \noindent
      \ttfamily #1
    \end{reviewer-comment~}
  }
  \par\noindent\ignorespaces
} { \bigskip\par }

\NewDocumentCommand \Reviewer { m } {
  \section*{Comments~by~Reviewer~#1}
}
\ExplSyntaxOff
\AtBeginDocument{\maketitle\thispagestyle{empty}\noindent}

% You can get probably get rid of these definitions:
\newcommand\meta[1]{$\langle\hbox{#1}\rangle$}
\newcommand\PaperTitle[1]{``\textit{#1}''}

\newtheorem{innerLemma}{Lemma}
\newenvironment{lemma}[1]
{\renewcommand\theinnerLemma{#1}\innerLemma}
{\endinnerLemma}

\title{Errata to ``Feature-Sharing in Cascade Detection Systems with Multiple Applications''}
\author{Long N. Le \and Douglas L. Jones}
\date{\today}

\begin{document}
	Dear Readers,
	
	If you are reading this document, I believe it is reasonable to presume that you have read the paper 

	\begin{itemize}
		\item Long N. Le, Douglas L. Jones. ``Feature-Sharing in Cascade Detection Systems with Multiple Applications.'' IEEE Journal of Selected Topics in Signal Processing, Special Issue on Cooperative Signal Processing for Heterogeneous and Multi-Task Wireless Sensor Networks, 2017.
	\end{itemize}

	and found it \textit{stimulating}, for better or worse. 
	As the first author of this paper, it is my responsibility to inform you that there are errors which made it through to the final version, despite our best effort and the industrious proofreading and checking done by reviewers and editors of the journal at the time being. 
	I am herein deeply sorry for any inconvenience that might have arise due to the errors.
	Below you will find details about them and the corresponding corrections to the original paper.
	
	Sincerely,
	
	Long N. Le
%This statement concerns our revision of the \meta{Paper ID} paper, entitled \PaperTitle{\meta{Paper Title}}, based on the referees' report.

\pagebreak

\section{Corrections for Equation (15)}
\begin{equation}\tag{15}\label{eqn:condForShare}
\begin{aligned}
C_M^2\left\{ \mathbb{E}[\pi_i^2(Y_i^1)]-\mathbb{E}[\pi_i^2(Y_i^2)] \right\} \leq \lambda D_{i}^2 ,i = 1,\dots,K
\end{aligned}
\end{equation}
Intuitively, \eqref{eqn:condForShare} requires that the expected increase in the secondary miss risk due to the shared/primary feature usage is relatively small compared to the resource cost of extracting the secondary feature. 

\section{Corrections for Lemmas 1.4 and 1.5}
\begin{lemma}{1.4}\label{lem:sharing}
	If the condition in \eqref{eqn:condForShare} holds, then
	\setcounter{equation}{37}
	\begin{equation}\label{eqn:42}
	\begin{aligned}
	\delta_i^{2\ast}(\pi_i^2;\pi_i^1) &= 
	\begin{cases}
	0, V_i^2 = \pi_i^2, \pi_i^1 < \tau_i^{1\ast}\\
	F^2, V_i^2 < \pi_i^2, \pi_i^1 < \tau_i^{1\ast}\\
	0, V_i^2 = \pi_i^2, \pi_i^1 \geq \tau_i^{1\ast}\\
	F^1, V_i^2 < \pi_i^2, \pi_i^1 \geq \tau_i^{1\ast}\\
	\end{cases},\\
	&i = 0,\dots,K-1
	\end{aligned}
	\end{equation}
	which implies $\delta_i^{2\ast} \neq F^2$ when $\pi_i^{1} \geq \tau_i^{1\ast}$.
	
	\begin{proof}
		The fourth expression of (31) is equivalent to Eq.\ \eqref{eqn:42} if and only if
		\begin{equation}\label{eqn:conditions}
		\mathbb{E}[V_{i}^2(Y_{i}^1,\pi_{i-1}^2)]- \mathbb{E}[V_{i}^2(Y_{i}^2,\pi_{i-1}^2)] \leq \lambda D_{i}^2\\
		\end{equation}
		
		The condition in \eqref{eqn:conditions} is made satisfied by \eqref{eqn:condForShare} because
		of Lemma \ref{lem:Ediffconcave} (note that $V_i^2$ is concave over $\pi_i^2$ for each $\pi_i^1$,$i=1,\dots,K$).
	\end{proof}
\end{lemma}

\begin{lemma}{1.5}\label{lem:Ediffconcave}
	\begin{equation}\label{eqn:bound1,chap:apx_sharing}
	\begin{aligned}
	\mathbb{E}[V_i(Y_i^1,\pi_{i-1})]- \mathbb{E}[V_i(Y_i^2,\pi_{i-1})] &\leq
	C_M^2 \left\{ \mathbb{E}[\pi_i(Y^1_i)] - \mathbb{E}[\pi_i(Y^2_i)]  \right\}\\
	&i = 1,\dots,K
	\end{aligned}
	\end{equation}

	\begin{proof}
		Since $V_i(\pi_i)$ is concave, $V_i'(\pi_i)$ is non-increasing. Furthermore, $V_i'(\epsilon) = C_M^2$ for some small $\epsilon > 0$. Therefore, $V_i'(\pi_i) \leq C_M^2$, i.e.\
		\begin{equation}\label{eqn:bound2,chap:apx_sharing}
		V_i(\pi_i(Y_i^1)) - V_i(\pi_i(Y_i^2)) \leq 
		C_M^2 \big[ \pi_i(Y_i^1)-\pi_i(Y_i^2) \big]
		\end{equation}
		
		Taking expectation on both size of \eqref{eqn:bound2,chap:apx_sharing} yields \eqref{eqn:bound1,chap:apx_sharing}.
	\end{proof}
\end{lemma}

\end{document}
