function [V,VE,VEK,VENK,VImi,VImiK,VImiNK,VIfa] = evalSys(cascade,thresh)
% [V,VE,VImi,VIfa] = evalSys(cascade,thresh)
% Evaluate the system risk given thresholds
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

K = numel(cascade.D); % number of stages
M = size(cascade.b,2); % number of state discretization

% convert into robust observation densities
[cascade] = robustify(cascade);
piL = [cascade.initbL zeros(1,K-1)];
piU = [cascade.initbU zeros(1,K-1)];
for k = 1:K-1
    [piL(k+1),piU(k+1)] = robustRange(cascade,k,piL(k),piU(k));
end

idx = nearestIdx(cascade.b,thresh(K));
V = zeros(M,K); % value functions
V(:,K) = [cascade.CM*cascade.b(1:idx)';cascade.CA*(1-cascade.b(idx+1:end)')];
VE = zeros(M,K); % value functions of energy
VE(:,K) = [cascade.CM*cascade.b(1:idx)'*0;cascade.CA*(1-cascade.b(idx+1:end)')*0];
VEK = zeros(M,K); % value functions of energy
VEK(:,K) = [cascade.CM*cascade.b(1:idx)'*0;cascade.CA*(1-cascade.b(idx+1:end)')*0];
VENK = zeros(M,K); % value functions of energy
VENK(:,K) = [cascade.CM*cascade.b(1:idx)'*0;cascade.CA*(1-cascade.b(idx+1:end)')*0];
VImi = zeros(M,K); % value functions of miss
VImi(:,K) = [cascade.CM*cascade.b(1:idx)';cascade.CA*(1-cascade.b(idx+1:end)')*0];
VImiK = zeros(M,K);
VImiK(:,K) = [cascade.CM*cascade.b(1:idx)';cascade.CA*(1-cascade.b(idx+1:end)')*0];
VImiNK = zeros(M,K);
VImiNK(:,K) = [cascade.CM*cascade.b(1:idx)'*0;cascade.CA*(1-cascade.b(idx+1:end)')*0];
VIfa = zeros(M,K); % value functions of false alarm
VIfa(:,K) = [cascade.CM*cascade.b(1:idx)'*0;cascade.CA*(1-cascade.b(idx+1:end)')];
for k = K-1:-1:1
    [J,JE,JEK,JENK,JImi,JImiK,JImiNK,JIfa] = eval(cascade,k,V,VE,VEK,VENK,VImi,VImiK,VImiNK,VIfa,piL,piU);
    
    %figure; hold on; plot(cascade.D(k+1)+J); plot(p)
    idx = nearestIdx(cascade.b,thresh(k));
    V(:,k) = [cascade.CM*cascade.b(1:idx)'+cascade.accDO(k+1);cascade.D(k+1)+J(idx+1:end)];
    V(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    
    VE(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1);cascade.D(k+1)+JE(idx+1:end)];
    VE(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    
    if k == K-1
        VEK(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1);cascade.D(k+1)+JEK(idx+1:end)];
        VEK(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
        
        VENK(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1)*0;0*cascade.D(k+1)+JENK(idx+1:end)];
        VENK(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    else
        VEK(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1)*0;0*cascade.D(k+1)+JEK(idx+1:end)];
        VEK(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
        
        VENK(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1);cascade.D(k+1)+JENK(idx+1:end)];
        VENK(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    end
    VImiK(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1)*0;JImiK(idx+1:end)];
    VImiK(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
        
    VImiNK(:,k) = [cascade.CM*cascade.b(1:idx)'+cascade.accDO(k+1)*0;JImiNK(idx+1:end)];
    VImiNK(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    
    VImi(:,k) = [cascade.CM*cascade.b(1:idx)'+cascade.accDO(k+1)*0;JImi(idx+1:end)];
    VImi(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    
    VIfa(:,k) = [cascade.CM*cascade.b(1:idx)'*0+cascade.accDO(k+1)*0;JIfa(idx+1:end)];
    VIfa(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
end
% energy cost of the first stage
[J,JE,JEK,JENK,JImi,JImiK,JImiNK,JIfa] = eval(cascade,0,V,VE,VEK,VENK,VImi,VImiK,VImiNK,VIfa,piL,piU);

% update V(0)
V0 = cascade.D(1)+J;
V0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VE0 = cascade.D(1)+JE;
VE0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VEK0 = 0*cascade.D(1)+JEK;
VEK0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VENK0 = cascade.D(1)+JENK;
VENK0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VImi0 = JImi;
VImi0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VImiK0 = JImiK;
VImiK0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VImiNK0 = JImiNK;
VImiNK0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

VIfa0 = JIfa;
VIfa0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

% append
V = [V0 V];
VE = [VE0 VE];
VEK = [VEK0 VEK];
VENK = [VENK0 VENK];
VImi = [VImi0 VImi];
VImiK = [VImiK0 VImiK];
VImiNK = [VImiNK0 VImiNK];
VIfa = [VIfa0 VIfa];

end

function [J,JE,JEK,JENK,JImi,JImiK,JImiNK,JIfa] = eval(cascade,k,V,VE,VEK,VENK,VImi,VImiK,VImiNK,VIfa,piL,piU) 
    L = size(cascade.P1{k+1},2); % size of the observation set
    M = size(cascade.b,2);
    Q = zeros(M,L); % future reward
    QE = zeros(M,L);
    QEK = zeros(M,L);
    QENK = zeros(M,L);
    QImi = zeros(M,L);
    QImiK = zeros(M,L);
    QImiNK = zeros(M,L);
    QIfa = zeros(M,L);
    for m = 1:M
        if cascade.b(m)>=piL(k+1) && cascade.b(m)<=piU(k+1)
            parfor l = 1:L
                [bb,pp] = stateObsProb(cascade,k+1,l,m);
                d = zeros(M,1);
                for mm = 1:M
                    d(mm) = norm(cascade.b(mm)-bb,1);
                end
                [~,idx] = min(d);
                Q(m,l) = pp*V(idx,k+1);
                QE(m,l) = pp*VE(idx,k+1);
                QEK(m,l) = pp*VEK(idx,k+1);
                QENK(m,l) = pp*VENK(idx,k+1);
                QImi(m,l) = pp*VImi(idx,k+1);
                QImiK(m,l) = pp*VImiK(idx,k+1);
                QImiNK(m,l) = pp*VImiNK(idx,k+1);
                QIfa(m,l) = pp*VIfa(idx,k+1);
            end
        end
    end
    J = sum(Q,2);
    JE = sum(QE,2);
    JEK = sum(QEK,2);
    JENK = sum(QENK,2);
    JImi = sum(QImi,2);
    JImiK = sum(QImiK,2);
    JImiNK = sum(QImiNK,2);
    JIfa = sum(QIfa,2);
end