% Read data from the figure and replot them
%
% Long Le
% University of Illinois
%

clear all; close all;

open('riskComponentsReal.fig');
h = gcf;
axesObjs = get(h, 'Children');  %axes handles
dataObjs = get(axesObjs, 'Children'); %handles to low-level graphics objects in axes

lambda = 0.005;

pMark = dataObjs{2}(1).XData;
faMark = dataObjs{2}(1).YData;
mMark = dataObjs{2}(2).YData;
wrcMark = dataObjs{2}(3).YData;

p = dataObjs{2}(4).XData;
fa = dataObjs{2}(4).YData;
m = dataObjs{2}(5).YData;
wrc = dataObjs{2}(6).YData;

figure; hold on;
plot(p(1),wrc(1),':bo','linewidth',3); plot(p(1),m(1),'-r+','linewidth',3); plot(p(1),fa(1),'--yv','linewidth',3);
plot(pMark,wrcMark,'bo'); plot(pMark,mMark,'r+'); plot(pMark,faMark,'yv');
xlabel('P(X=1)'); ylabel('Risk'); title(['\lambda = ' num2str(lambda)])
plot(p,wrc,':b'); plot(p,m,'-r'); plot(p,fa,'--y');
legend('Weighted Resource Cost','Miss Rate','False-Alarm Rate','location','northwest');
grid on;