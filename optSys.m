function [thresh,V] = optSys(cascade)
% [thresh,V] = optSys(cascade)
% Find optimal thresholds/operating points
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

K = numel(cascade.D); % number of stages
M = size(cascade.b,2); % number of state discretization

% convert into robust observation densities
[cascade] = robustify(cascade);
piL = [cascade.initbL zeros(1,K-1)];
piU = [cascade.initbU zeros(1,K-1)];
for k = 1:K-1
    [piL(k+1),piU(k+1)] = robustRange(cascade,k,piL(k),piU(k));
end

V = zeros(M,K); % value functions
V(:,K) = min(cascade.CM*cascade.b,cascade.CA*(1-cascade.b),'includenan');
thresh = zeros(1,K);
thresh(K) = cascade.CA/(cascade.CA+cascade.CM);
for k = K-1:-1:1
    J = eval(cascade,k,V,piL,piU);
    
    % update V(k)
    V(:,k) = min(cascade.CM*cascade.b'+cascade.accDO(k+1),cascade.D(k+1)+J,'includenan');
    V(cascade.b<piL(k+1) | cascade.b>piU(k+1),k) = NaN;
    
    % solve for thresh(k)
    %figure; hold on; plot(cascade.D(k+1)+sum(J,2)); plot(p)
    fd = V(:,k)-(cascade.CM*cascade.b'+cascade.accDO(k+1)); % diff function
    idx = find(fd<0,1);
    thresh(k) = cascade.b(idx);
    
	% check for standard cascade optimality
    fd = V(:,k)-(cascade.CA*(1-cascade.b')+cascade.accDO(k+1));
    idx = find(fd<0,1,'last');
    if cascade.b(idx) < piU(k+1)
        warning('stage %d in the cascade is NOT optimal!',k);
    else
        fprintf(1,'stage %d in the cascade is optimal!\n',k);
    end
end
% energy cost of the first stage
J = eval(cascade,0,V,piL,piU);

% update V(0)
V0 = cascade.D(1)+J;
V0(cascade.b<piL(1) | cascade.b>piU(1)) = NaN;

V = [V0 V];
end

function J = eval(cascade,k,V,piL,piU) 
    L = size(cascade.P0{k+1},2); % assume P0 and P1 has the same size 
    M = size(cascade.b,2);
    Q = zeros(M,L); % future reward
    parfor m = 1:M
        if cascade.b(m)>=piL(k+1) && cascade.b(m)<=piU(k+1) % pi_k
            for l = 1:L
                [bb,pp] = stateObsProb(cascade,k+1,l,m); % E[Y_{k+1}]
                % the delta trick to adapt the notion of the 
                % transition prob/function from the  fully observable setting 
                % to the partially observable one
                % See Braziunas' POMDP solution methods
                idx = nearestIdx(cascade.b,bb);
                Q(m,l) = pp*V(idx,k+1);
            end
        end
    end
    J = sum(Q,2);
end
