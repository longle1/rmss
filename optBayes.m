function [thresh,V] = optBayes(detector)
% [thresh,V] = optBayes(detector)
% Find Bayes optimal thresholds/operating point for a detector
%
% Long Le <longle1@illinois.edu>
% University of Illinois
%

thresh = detector.CA/(detector.CA+detector.CM);

M = size(detector.b,2);

[lF0,lF1,sL] = likCumsum(detector.P0,detector.P1);

V = zeros(M,1);
for m = 1:M
    tau = max(min(sL),min(max(sL),thresh*(1-detector.b(m))/detector.b(m)));
    [~,idx] = min(abs(tau - sL));    
    V(m) = (1-lF0(idx))*(1-detector.b(m)) + lF1(idx)*detector.b(m);
end