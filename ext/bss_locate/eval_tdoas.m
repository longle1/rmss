function [R, P, F, Acc] = eval_tdoas(taue, tau, d, thres)

% EVAL_TDOAs Evaluation of TDOA estimation in terms of recall, precision,
% F-measure and accuracy
%
% [R, P, F, Acc] = eval_tdoas(taue, tau, thresh)
%
% Inputs:
% taue: 1 x nsrce vector of estimated TDOAs
% tau: 1 x nsrc vector of true TDOAs
% d: microphone spacing in meters
% thresh: correctness threshold in degrees under the far-field assumption
%
% Outputs:
% R: recall
% P: precision
% F: F-measure
% Acc: 1 x nsrce vector of accuracies in degrees (+inf if above the
% threshold)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2010-2011 Charles Blandin and Emmanuel Vincent
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt)
% If you find it useful, please cite the following reference:
% Charles Blandin, Emmanuel Vincent and Alexey Ozerov, "Multi-source TDOA
% estimation in reverberant audio using angular spectra and clustering",
% Signal Processing, to appear.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Greedy matching of the estimated TDOAs with the true TDOAs
nsrce = length(taue);
nsrc = length(tau);
gtd = inf(1,nsrce);
temp = tau;
tempe = taue;
for i = 1:min(nsrce,nsrc),
    dist = abs(repmat(temp',1,length(tempe))-repmat(tempe,length(temp),1));
    [dist,ind] = min(dist(:));
    [k,l] = ind2sub([length(temp),length(tempe)],ind);
    gtd(l) = temp(k);
    tempe(l) = inf;
    temp(k) = [];
end

% Computing the metrics
taue = acosd(343/d*taue);
gtd = acosd(343/d*gtd);
correct = (abs(gtd-taue) <= thres);
R = sum(correct)/nsrc;
P = sum(correct)/nsrce;
F = 2*(P.*R)./(P + R + realmin);
Acc = inf(1,nsrce);
Acc(correct) = abs(taue(correct) - gtd(correct));

return;