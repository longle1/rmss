% Cascade on real data(GCWA)
%
% Long Le <longle1@illinois.edu>
% University of Illinois

clear all; close all;

addpath(genpath('../voicebox/'))
addpath(genpath('../jsonlab/'))
addpath(genpath('../V1_1_urlread2'));
addpath(genpath('../sas-clientLib/src'));
addpath(genpath('../node-paper'));
addpath(genpath('../cohmm'));

%servAddr = '128.32.33.227';
servAddr = 'acoustic.ifp.illinois.edu';
DB = 'publicDb';
USER = 'nan';
PWD = 'publicPwd';
DATA = 'data';
EVENT = 'event';

%% Download detected raw data
%load('../node-paper/localLogs/events_20160118.mat');
fs = 16e3;
blockSize = 256;

allY = cell(numel(events),1);
parfor l = 1:numel(events)
    disp(l);
    
    % download the binary data
    data = IllGridGet(servAddr, DB, USER, PWD, DATA, events{l}.filename);
    try
        [y, header] = wavread_char(data);
    catch e
        disp('missing binary data')
        %resp = IllColDelete(servAddr, DB, USER, PWD, EVENT, events{l}.filename);
        %disp(resp);
        continue;
    end
    allY{l} = y';
    %{
    [S,tt,ff] = mSpectrogram(y,fs,blockSize);
    
    % display data
    figure; hold on
    imagesc(tt, ff, log(abs(S)));
    axis xy; axis tight
    
    % play sound
    sound(y, double(header.sampleRate));
    %}
end

%% feature extraction
%load('localLogs/allY_20160118.mat');
frameSize = 2^floor(log2(0.03*fs));

lab = zeros(1,numel(allY));
featTFRidge = cell(1,numel(allY));
featMFCC = cell(1,numel(allY));
parfor k = 1:numel(allY)
    % labels
    if isfield(events{k}, 'tag')
        lab(k) = sound2labId(events{k}.tag);
    else
        lab(k) = -1;
    end
        
    % TF Ridge
    if isfield(events{k},'TFRidgeFeat')
        featTFRidge{k} = events{k}.TFRidgeFeat;
    elseif isfield(events{k},'feat') % backward compatible
        featTFRidge{k} = events{k}.feat;
    else
        disp('event missing feat or TFRidgeFeat');
    end
    
    % MFCC
    y = allY{k};
    if ~isempty(y)
        featMFCC{k} = melcepst(y,fs,'Mtaz',3,floor(3*log(fs)),frameSize)';
        %{
        % TODO: Use the best PCA-MFCC
        cepstCoef = melcepst(y,fs,'Mtaz',16,floor(3*log(fs)),frameSize);
        [pcaCoeff, pcaScore] = pca(cepstCoef);
        featMFCC{k} = (pcaScore(:,1:3)*pcaCoeff(1:3,1:3)'+repmat(mean(cepstCoef(:,1:3)),size(cepstCoef,1),1))';
        %}
    end
end
%save('localLogs/feat.mat','lab','featTFRidge','featMFCC');

%% classification
%load('../node-paper/localLogs/classLoss_2016-01-22-14-21-28-781_1.mat','svmModel');
%load('../cohmm/localLogs/newCohmm2.mat','newCohmm');

% Using TF Ridge feature
mSVMModel = svmModel{6};
if strcmp(mSVMModel.ScoreTransform,'none')
    mSVMModel = fitPosterior(mSVMModel, mSVMModel.X, mSVMModel.Y);
end
prob = zeros(1,size(featTFRidge,2));
tic
parfor k = 1:size(featTFRidge,2)
    [~,score] = predict(mSVMModel,featTFRidge{k})
    prob(k) = score(2);
end
elapsedTime(1) = toc;

figure; 
ax(1)=subplot(311);plot(prob)
ax(2)=subplot(312);plot(lab)
ax(3)=subplot(313);plot(prob>=0.019);ylim([0 1.2])
linkaxes(ax,'x')

% Using MFCC feature
estStates = cell(1,size(featMFCC,2));
logProb = zeros(1,size(featMFCC,2));
tic
parfor k = 1:size(featMFCC,2)
    fprintf(1,'At iteration %d\n',k);
    if isempty(featMFCC{k})
        continue;
    end
    
    estStates{k} = cohmmViterbi(newCohmm,featMFCC{k});
    logProb(k) = cohmmForwBack(newCohmm,featMFCC{k});
end
elapsedTime(2) = toc;
[nr,nc] = cellfun(@size,featMFCC);
nLogProb = logProb./nc;

figure; 
ax(1)=subplot(311);plot(nLogProb)
ax(2)=subplot(312);plot(lab)
ax(3)=subplot(313);plot(nLogProb >= -4.891);ylim([0 1.2])
linkaxes(ax,'x')

%% Find optimal configuration for the 2-stage cascade
K = 2;
L = [10 15];
n1 = cell(1,K);
x1 = cell(1,K);
n0 = cell(1,K);
x0 = cell(1,K);
for k = 1:K
    if k==1
        y1 = prob(lab==12);
        y0 = prob(lab~=12);
    elseif k==2
        y1 = nLogProb(lab==12);
        y0 = nLogProb(lab~=12);
    end
    yRange = [min([y1 y0]):range([y1 y0])/(L(k)-1):max([y1 y0])];
    [n1{k},x1{k}] = hist(y1,yRange);
    [n0{k},x0{k}] = hist(y0,yRange);
    n1{k}(n1{k}==0) = eps;
    n0{k}(n0{k}==0) = eps;
    figure; bar(union(x0{k},x1{k}),[n0{k}'/sum(n0{k}) n1{k}'/sum(n1{k})]);
end

lambda = 0.005;
cascade.D = [2 140]*lambda;
for k = 1:numel(cascade.D)
    cascade.P1{k} = n1{k}/sum(n1{k});
    cascade.P0{k} = n0{k}/sum(n0{k});
end
[thresh,V] = optSys(cascade);

q = 0:1/(numel(V(:,1))-1):1;
[V2,VE,VI,VImi,VIfa] = evalSys(cascade,thresh);
figure; hold on;
plot(q,VE(:,1)); plot(q,VImi(:,1)); plot(q,VIfa(:,1));
xlabel('P(X=1)'); ylabel('Risk'); title(['\lambda = ' num2str(lambda)])
legend('Weighted Resource Cost','Miss Rate','False-Alarm Rate','location','northwest');

%% comparison with exhausted search
M = 1e1;
p = (0:1/(M-1):1)';
V2 = cell(M,M);
parfor k = 1:M
    for l = 1:M
        V2{k,l} = evalSys(cascade,[p(k) p(l)]);
    end
end

X = [0:1/999:1];
figure; hold on;
plot(q,V(:,1),'linewidth',5,'color','b')
for k = 1:M
    for l = 1:M
        plot(X,V2{k,l}(:,1))
    end
end
xlabel('P(X=1)'); ylabel('System Risk'); title(['\lambda = ' num2str(lambda)])
legend('The optimal threshold')
