function x = mix(fname, stype, G)

% MIX Generates mixtures for benchmarking
%
% x = mix(fname, stype)
%
% Inputs
% fname: BSS Locate mixing filter filename
% stype: BSS Locate source type ('female', 'male' or 'music')
% G: source gain
%
% Output
% x: 2 x 190218 mixture signal
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2010-2011 Charles Blandin and Emmanuel Vincent
% This software is distributed under the terms of the GNU Public License
% version 3 (http://www.gnu.org/licenses/gpl.txt)
% If you find it useful, please cite the following reference:
% Charles Blandin, Emmanuel Vincent and Alexey Ozerov, "Multi-source TDOA
% estimation in reverberant audio using angular spectra and clustering",
% Signal Processing, to appear.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Loading the data
load(fname, 'A');
nsrc = str2double(fname(1));
s = zeros(190218, nsrc);
for j = 1:nsrc,
    s(:,j) = G(j)*wavread([stype '_s' int2str(j) '.wav']);
end

% Mixing
simg = zeros(2,nsrc,190218);
for i = 1:2, 
    for j = 1:nsrc,
        simg(i,j,:) = fftfilt(A(i,j,:),s(:,j));
    end
end
x = squeeze(sum(simg,2));

return;