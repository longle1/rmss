y = randn(1e6,2);
h = [1,0];

s1 = sum(y.^2,2);
s2 = y*h';
s2t = s2(s1 > 0.5);
s22 = y(s1 > 0.5,:)*h';
s22t = s22;

% verify that the distribution has a hole in the center
figure; hist3(y(s1 > 0.5,:),'nbins',[1e2,1e2])

%[F2,X2] = ecdf(s2);
%[F2t,X2t] = ecdf(s2t);

% (standard) empirical cdf of the full and truncated
%figure; hold on;
%plot(X2,F2); plot(X2t,F2t)

[N1,X1] = hist(s1,1e3);
[N2,X2] = hist(s2,1e3);
[N2t,X2t] = hist(s2t,1e3);
[N22,X22] = hist(s22,1e3);
[N22t,X22t] = hist(s22t,1e3);

% (standard) empirical pdf of the full and truncated.
% Note that there is no hole in the distribution of X2t (truncated)
%figure;
%plot(N1./sum(N1),X1);
figure; hold on;
plot(X2,N2./sum(N2)); plot(X2t,N2t./sum(N2t))
plot(X22,N22./sum(N22)); plot(X22t,N22t./sum(N22t))
legend('2','2t','22','22t')