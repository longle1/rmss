function [newCascade] = robustify(cascade)
% [newCascade] = robustify(cascade)
%
% Long le
% University of Illinois
%
K = numel(cascade.D);

if isfield(cascade,'eps0')&&isfield(cascade,'eps1')&&isfield(cascade,'nu0')&&isfield(cascade,'nu1')
    for k = 1:K-1
        % distance to the optimal/best stage
        params.eps0 = cascade.eps0(k);
        params.eps1 = cascade.eps1(k);
        params.nu0 = cascade.nu0(k);
        params.nu1 = cascade.nu1(k);
        [cascade.P0{k},cascade.P1{k}] = robustDist(cascade.P0{k},cascade.P1{k},params);
    end
end

newCascade = cascade;