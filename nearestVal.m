function val = nearestVal(p,pVal)
% val = nearestVal(p,pVal)
%
% Long Le
% University of Illinois
%
    idx = nearestIdx(p,pVal);
    val = p(idx);
end
